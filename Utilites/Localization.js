import I18n from 'ex-react-native-i18n';
import Expo from 'expo';
import { DangerZone } from 'expo';

I18n.initAsync = async () => {
  const locale = await Expo.DangerZone.Localization.getCurrentLocaleAsync();
  I18n.locale = (locale) ? locale.replace(/_/, '-') : '';
}

I18n.fallbacks = true
I18n.translations = {
  en: {
    //экран входа
    wellcome1:             'Нет времени на разговоры?',
    subtitle1:             'Запишитесь к лучшему мастеру в один клик без лишних слов',
    wellcome2:             'Снова недовольны стрижкой?',
    subtitle2:             'Узнайте что думают другие клиенты о мастере, которого вы хотите посетить',
    wellcome3:             'Опять забыли про маникюр?',
    subtitle3:             'Получайте оповещения о предстоящих визитах к мастеру заблаговременно',
    telegramButton:       'Продолжить с Telegram',
    whatsappButton:       'Продолжить с Whatsapp',
    viberButton:          'Продолжить с Viber',
    phoneButton:          'Продолжить с номером телефона',
    //экран авторизации
    authorization:        'Авторизация',
    authTitle:            'Для авторизации в BeautyLook Вы можете использовать ваш номер телефона',
    authSubtitlePhone:    'Пожалуйста, укажите ваш номер в международном формате и мы отправим SMS на ваш номер',
    authSubtitleTelegram: 'Пожалуйста, укажите ваш номер в международном формате и мы отправим SMS на ваш аккаунт Telegram',
    authSubtitleWhatsapp: 'Пожалуйста, укажите ваш номер в международном формате и мы отправим SMS на ваш аккаунт Whatsapp',
    authSubtitleViber:    'Пожалуйста, укажите ваш номер в международном формате и мы отправим SMS на ваш аккаунт Viber',
    //экран ввода кода смс
    codeTitlePhone:       'Введите sms-код, отправленный на номер',
    codeTitleTelegram:    'Введите sms-код, отправленный в Telegram на номер',
    codeTitleWhatsapp:    'Введите sms-код, отправленный в Whatsapp на номер',
    codeTitleViber:       'Введите sms-код, отправленный в Viber на номер',
    notSms:               'Не получили SMS?',
    sendCodeAfter:        'Отправить код можно через ',
    secunds:              ' сек.',
    sendCodeNew:          'Отправить код повторно',
    //экран каталога
    catalogTitle:         'Категория услуг',
    //Экран Мои записи
    myNotes:              'Мои записи',
    prevNotes:            'Прошедшие',
    futureNotes:          'Предстоящие',
    emptyTitleNotes:      'Если не знаете, с чего начать, начните с начала',
    questionNotes:        'Хотите добавить первую запись?',
    addNewNotes:          'Добавить новую запись',
    //Меню
    settings:             'Настройки',
    share:                'Поделиться с друзьями',
    support:              'Служба поддержки',
    beautyLookBussiness:  'BeautyLook Bussiness',
    exit:                 'Выйти',
    clientAgreement:      'Клиентское соглашение',
    policyConfident:      'Политика конфиденциальности',
    //экран описания услуги
    usluga:               'Услуга',
    master:               'Мастер',
    comment:              'Комментарий',
    cancelUsluga:         'Отменить запись',
    //Экран Избранное
    favor:                'Избранное',
    saloons:              'Салоны',
    masters:              'Мастера',
    favorEmptyTitle:      'У Вас нет салонов в избранном',
    favorEmptySubtitle:   'Добавляйте салоны и мастеров в избранное из каталога',
    favorEmptyButton:     'К каталогу',
    zapis:                'Запись',
    //Редактирование профиля
    editProfile:          'Редактировать профиль',
    name:                 'Имя',
    lastname:             'Фамилия',
    phone:                'Телефон',
    address:              'Адрес (для заказа мастера на дом)',
    city:                 'Город',
    street:               'Улица',
    house:                'Дом',
    building:             'Строение',
    korpus:               'Корпус',
    flat:                 'Квартира',
    stage:                'Этаж',
    profileSaved:         'Изменения профиля сохранены!',
    makePhoto:            'Сделать фото',
    gallery:              'Галерея',
    cancel:               'Отмена',
    //Экран настроек
    rememberNote:         'Напоминание о записи',
    language:             'Язык',
    notifyOff:            'Отключено',
    notify30m:            'за 30 минут',
    notify1h:             'за 1 час',
    notify2h:             'за 2 часа',
    notify3h:             'за 3 часа',
    notifyOwn:            'Свое время',
    notifyImportant:      'Важно: Чтобы получать уведомления, должны быть включены push-уведомления для BeautyLook в настройках телефона',
    langEn:               'Английский',
    langRu:               'Русский',
    langKz:               'Казахский',
    langUa:               'Украинский',
    //Экран салона
    saloon:               'Салон',
    comments:             'Отзывы',
    //MapScreen
    note:                 'Записаться',
    //Common screen
    futureNotes2:         'Предстоящее',
    toAll:                'Ко всем',
    //Экран информации о смене номера телефона
    changePhoneInfo:      'Здесь Вы можете сменить номер телефона. Ваш аккаунт и все данные (записи, списки избранного и т.д.) будут перенесены на новый номер.',
    //экран ввода номера для смены номера
    changePhoneTitle:     'Активация нового номера телефона через:',
    changePhoneText:      'Новый номер телефона должен быть подключен к выбранному способу активации.',
    //экран смены города в каталоге
    catalog:              'Каталог',
    changeCityPlace:      'Местоположение',
    changeCityText:       'Укажите город для коректного отображения результатов поиска',
    selectCity:           'Местоположение',
  },
  ru: {
    //экран входа
    wellcome1:             'Нет времени на разговоры?',
    subtitle1:             'Запишитесь к лучшему мастеру в один клик без лишних слов',
    wellcome2:             'Снова недовольны стрижкой?',
    subtitle2:             'Узнайте что думают другие клиенты о мастере, которого вы хотите посетить',
    wellcome3:             'Опять забыли про маникюр?',
    subtitle3:             'Получайте оповещения о предстоящих визитах к мастеру заблаговременно',
    telegramButton:       'Продолжить с Telegram',
    whatsappButton:       'Продолжить с Whatsapp',
    viberButton:          'Продолжить с Viber',
    phoneButton:          'Продолжить с номером телефона',
    //экран авторизации
    authorization:        'Авторизация',
    authTitle:            'Для авторизации в BeautyLook Вы можете использовать ваш номер телефона',
    authSubtitlePhone:    'Пожалуйста, укажите ваш номер в международном формате и мы отправим SMS на ваш номер',
    authSubtitleTelegram: 'Пожалуйста, укажите ваш номер в международном формате и мы отправим SMS на ваш аккаунт Telegram',
    authSubtitleWhatsapp: 'Пожалуйста, укажите ваш номер в международном формате и мы отправим SMS на ваш аккаунт Whatsapp',
    authSubtitleViber:    'Пожалуйста, укажите ваш номер в международном формате и мы отправим SMS на ваш аккаунт Viber',
    //экран ввода кода смс
    codeTitlePhone:       'Введите sms-код, отправленный на номер',
    codeTitleTelegram:    'Введите sms-код, отправленный в Telegram на номер',
    codeTitleWhatsapp:    'Введите sms-код, отправленный в Whatsapp на номер',
    codeTitleViber:       'Введите sms-код, отправленный в Viber на номер',
    notSms:               'Не получили SMS?',
    sendCodeAfter:        'Отправить код можно через ',
    secunds:              ' сек.',
    sendCodeNew:          'Отправить код повторно',
    //экран каталога
    catalogTitle:         'Категория услуг',
    //Экран Мои записи
    myNotes:              'Мои записи',
    prevNotes:            'Прошедшие',
    futureNotes:          'Предстоящие',
    emptyTitleNotes:      'Если не знаете, с чего начать, начните с начала',
    questionNotes:        'Хотите добавить первую запись?',
    addNewNotes:          'Добавить новую запись',
    //Меню
    settings:             'Настройки',
    share:                'Поделиться с друзьями',
    support:              'Служба поддержки',
    beautyLookBussiness:  'BeautyLook Bussiness',
    exit:                 'Выйти',
    clientAgreement:      'Клиентское соглашение',
    policyConfident:      'Политика конфиденциальности',
    //экран описания услуги
    usluga:               'Услуга',
    master:               'Мастер',
    comment:              'Комментарий',
    cancelUsluga:         'Отменить запись',
    //Экран Избранное
    favor:                'Избранное',
    saloons:              'Салоны',
    masters:              'Мастера',
    favorEmptyTitle:      'У Вас нет салонов в избранном',
    favorEmptySubtitle:   'Добавляйте салоны и мастеров в избранное из каталога',
    favorEmptyButton:     'К каталогу',
    zapis:                'Запись',
    //Редактирование профиля
    editProfile:          'Редактировать профиль',
    name:                 'Имя',
    lastname:             'Фамилия',
    phone:                'Телефон',
    address:              'Адрес (для заказа мастера на дом)',
    city:                 'Город',
    street:               'Улица',
    house:                'Дом',
    building:             'Строение',
    korpus:               'Корпус',
    flat:                 'Квартира',
    stage:                'Этаж',
    profileSaved:         'Изменения профиля сохранены!',
    makePhoto:            'Сделать фото',
    gallery:              'Галерея',
    cancel:               'Отмена',
    //Экран настроек
    rememberNote:         'Напоминание о записи',
    language:             'Язык',
    notifyOff:            'Отключено',
    notify30m:            'за 30 минут',
    notify1h:             'за 1 час',
    notify2h:             'за 2 часа',
    notify3h:             'за 3 часа',
    notifyOwn:            'Свое время',
    notifyImportant:      'Важно: Чтобы получать уведомления, должны быть включены push-уведомления для BeautyLook в настройках телефона',
    langEn:               'Английский',
    langRu:               'Русский',
    langKz:               'Казахский',
    langUa:               'Украинский',
    //Экран салона
    saloon:               'Салон',
    comments:             'Отзывы',
    //MapScreen
    note:                 'Записаться',
    //Common screen
    futureNotes2:         'Предстоящее',
    toAll:                'Ко всем',
    //Экран информации о смене номера телефона
    changePhoneInfo:      'Здесь Вы можете сменить номер телефона. Ваш аккаунт и все данные (записи, списки избранного и т.д.) будут перенесены на новый номер.',
    //экран ввода номера для смены номера
    changePhoneTitle:     'Активация нового номера телефона через:',
    changePhoneText:      'Новый номер телефона должен быть подключен к выбранному способу активации.',
    //экран смены города в каталоге
    catalog:              'Каталог',
    changeCityPlace:      'Местоположение',
    changeCityText:       'Укажите город для коректного отображения результатов поиска',
    selectCity:           'Местоположение',
  }
}

export default I18n;
