import React from 'react';
import {observable} from 'mobx';
import * as mobx from 'mobx';
import {Platform} from 'react-native';
import Config from './../constants/Config';
import AsyncStorage from '@react-native-async-storage/async-storage';

class Network extends React.Component {

  @observable access_token = '';
  @observable userProfile = null;
  @observable.shallow ordersList = [];
  @observable storyIndex = 0;
  @observable selectedDevice = null;
  @observable maxDegA = 0;

  @observable minDegA = 0;
  @observable nowDegA = 0;
  @observable maxDegB = 0;
  @observable minDegB = 0;
  @observable nowDegB = 0;
  @observable limitA = 0;
  @observable limitB = 0;
  @observable nowLoadA = 0;
  @observable nowLoadB = 0;
  @observable nowForce = 0;
  @observable velocity = 0;
  @observable mode = 0;
  @observable nowMode = 0;
  @observable time = 0;
  @observable nowTime = 0;
  @observable pause = 0;
  @observable part = 0;
  @observable pacient = 0;
  @observable statusA = 0;
  @observable statusB = 0;
  @observable nonactiveDegA = 0;
  @observable nonactiveDegB = 0;
  @observable loadNegative = 0;
  @observable loadPositive = 0;
  @observable nonPrepareA = 0;
  @observable nonPrepareB = 0;
  @observable signalA = 0;
  @observable signalB = 0;
  @observable error = 0;
  @observable isChanged = 0;
  @observable directA = 0;
  @observable directB = 0;
  @observable inWork = 0;
  @observable inPause = 0;
  @observable inStart = 0;
  @observable inStop = 0;
  @observable inHome = 0;
  @observable inMain = 0;
  @observable inUp = 0;
  @observable inDown = 0;
  @observable inHot = 0;
  @observable inTrain = 0;
  @observable inComfort = 0;
  @observable inInverse = 0;
  @observable inHoldA = 0;
  @observable inHoldB = 0;
  @observable hasUsb = 0;
  @observable inActive = 0;
  @observable inHotActive = 0;
  @observable inFreeActive = 0;
  @observable currentDevice = null;
  @observable queue = [];
  
  exitFun = null;
  mainRefresh = null;
  refreshList = null;

  storyScroll = React.createRef();
  direction = 1;
  storyItemWidth = [];
  navigation = null;

  constructor(props) {
    super(props);
    mobx.autorun(() => {});
    
  }
}

const network = new Network();
export default network;

export function getCode(phone) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/auth/get-code/' + phone, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      // console.warn('response: '+JSON.stringify(response));
      response.json().then(data => {
        console.warn('getCode: ' + JSON.stringify(data));
        if (!data.success) {
          reject(data.error);
        } else {
          resolve(data);
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function sendCode(phone, code) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/auth/sign-in/' + phone + '/' + code, {
      method: 'get',
      headers: {
        // 'Authorization': 'JWT ' + network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      // console.warn('response: '+JSON.stringify(response));
      response.json().then(data => {
        console.warn('sendCode: ' + JSON.stringify(data));
        if (!data.success) {
          reject(data.error);
        } else {
          AsyncStorage.setItem('token', data.token);
          network.access_token = data.token;
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function login(phone, password) {

  return new Promise(function(resolve, reject) {

    let formdata = new FormData();
    
    formdata.append("phone", phone);
    formdata.append("password", password);

    fetch(Config.apiDomain + '/auth/sign-in', {
      method: 'post',
      headers: {
        // 'Authorization': 'JWT ' + network.access_token,
        // 'Content-Type': 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: formdata,
    })
    .then(response => {
      // console.warn('response: '+JSON.stringify(response));
      response.json().then(data => {
        console.warn('login: ' + JSON.stringify(data));
        if (!data.success) {
          reject(data.error.password);
        } else {
          AsyncStorage.setItem('token', data.token);
          network.access_token = data.token;
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getProfile() {

  return new Promise(function(resolve, reject) {
    console.warn(Config.apiDomain + '/profile/index/' + network.access_token);
    fetch(Config.apiDomain + '/profile/index/' + network.access_token, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('getProfile: ' + JSON.stringify(data));
        if (data.id === null || data.id === undefined) {
          reject(data.error);
        } else {
          network.userProfile = data;
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function saveProfile(profile) {

  return new Promise(function(resolve, reject) {
    let str = '';

    if (profile.surname) {
      str = str + (str.length ? '&' : '');
      str = str + 'surname=' + profile.surname;
    }
    if (profile.name) {
      str = str + (str.length ? '&' : '');
      str = str + 'name=' + profile.name;
    }
    if (profile.patronymic) {
      str = str + (str.length ? '&' : '');
      str = str + 'patronymic=' + profile.patronymic;
    }
    if (profile.phone) {
      str = str + (str.length ? '&' : '');
      str = str + 'phone=' + profile.phone;
    }
    if (profile.password) {
      str = str + (str.length ? '&' : '');
      str = str + 'password=' + profile.password;
    }
    if (profile.password_confirm) {
      str = str + (str.length ? '&' : '');
      str = str + 'password_confirm=' + profile.password_confirm;
    }
    fetch(Config.apiDomain + '/profile/update/' + network.access_token + '?' + str, {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      // console.warn('saveProfile: ' + JSON.stringify(response));
      response.json().then(data => {
        console.warn('saveProfile: ' + JSON.stringify(data));
        if (!data.success) {
          let message = '';
          let keys = Object.keys(data.error);
          for (let i = 0; i < keys.length; i++) {
            message = message + data.error[keys[i]] + ' ';
          }
          reject(message);
        } else {
          resolve(data.message);
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function sendCoords(latitude, longitude) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/api/setgeo/' + network.access_token + '/?latitude=' + latitude + '&longitude=' + longitude, {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      // console.warn('saveProfile: ' + JSON.stringify(response));
      response.json().then(data => {
        console.warn('sendCoords: ' + JSON.stringify(data));
        if (!data.success) {
          reject(data.error);
        } else {
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getOrders() {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/request/index/' + network.access_token, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      response.json().then(data => {
        console.warn('getOrders: ' + JSON.stringify(data));
        if (!data.success) {
          network.ordersList = [];
          reject(data.error);
        } else {
          network.ordersList = data.orders;
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function changeStatus(order_id, status, type_pay = null) {

  return new Promise(function(resolve, reject) {
    let str = '';
    if (type_pay !== null) {
      str = '/' + type_pay;
    }
    console.warn(Config.apiDomain + '/request/change-status/' + network.access_token + '/' + order_id + '/' + status + str);
    fetch(Config.apiDomain + '/request/change-status/' + network.access_token + '/' + order_id + '/' + status + str, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      console.warn('changeStatus response: ' + JSON.stringify(response));
      response.json().then(data => {
        console.warn('changeStatus: ' + JSON.stringify(data));
        if (!data.success) {
          reject(data.error);
        } else {
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function setPushId(push_id) {
  return new Promise(function(resolve, reject) {
    console.warn(Config.apiDomain + '/profile/pushtoken/' + network.access_token + '/' + push_id);
    fetch(Config.apiDomain + '/profile/pushtoken/' + network.access_token + '/' + push_id, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      // console.warn('setPushId response: ' + JSON.stringify(response));
      response.json().then(data => {
        console.warn('setPushId: ' + JSON.stringify(data));
        if (!data.success) {
          reject(data.error);
        } else {
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}