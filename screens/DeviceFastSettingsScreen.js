import React from 'react';
import {
  Platform,
  Text,
  View,
  BackHandler,
  Alert,
  RefreshControl,
  Linking,
  Image,
  NativeModules,
  NativeEventEmitter,
  TouchableOpacity,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
import OneMotorView from '../components/DeviceFastSettings/OneMotorView';
import TwoMotorView from '../components/DeviceFastSettings/TwoMotorView';

@observer
export default class DeviceFastSettingsScreen extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    headerTitle: (
      <View style={{
        // width: 306,
        flex: 1,
        height: Platform.OS === "android" ? 48 : 44,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
        <Text style={{
          color: 'white',
          fontFamily: 'OpenSans-Semibold',
          fontWeight: '600',
          textAlign: 'center',
          fontSize: Common.getLengthByIPhone7(16),
          lineHeight: Common.getLengthByIPhone7(22),
          letterSpacing: -0.3,
        }}
        allowFontScaling={false}>
            {Network.currentDevice.name} s/n{Network.currentDevice.serial}
        </Text>
      </View>
    )
  });

  timer = null;

  state = {
    loading: false,
    reload: false,
    statusA: 0,
    statusB: 0,
    showLoad: false,
    showSpeed: false,
    showPause: false,
    scrollEnabled: true,
    open: false,
    date: new Date(),
    maxADeg: 175,
    minADeg: 30,
    maxBDeg: 90,
    minBDeg: -90,
  };

  UNSAFE_componentWillMount() {

    this.setState({
      loading: true,
    });
    // Network.sendCommand('get_devpar 30\r\n');//maxA
    Network.addComand('get_devpar 30\r\n');
    
    this.props.navigation.addListener('willFocus', () => {
      BackHandler.addEventListener(
        "hardwareBackPress",
        () => {
          this.props.navigation.goBack(null);
          return true;
        }
      );
    });

    Network.reloadFun = this.reloadFun;
    Network.setMinMax = this.setMinMax;
    this.setState({
      statusA: Network.statusA,
      statusB: Network.statusB,
    });

    this.timer = setInterval(() => {
      // Network.sendCommand('get_prcsettings\r\n');
      Network.addComand('get_prcsettings\r\n');
    }, 5000);
  }

  UNSAFE_componentWillUnmount() {
    clearInterval(this.timer);
  }

  constructor(props) {
    super(props);
  }

  setMinMax = (param, value) => {
    if (param == 30) {
      this.setState({
        maxADeg: parseInt(value),
      });
      // Network.sendCommand('get_devpar 31\r\n');//maxB
      Network.addComand('get_devpar 31\r\n');
    } else if (param == 31) {
      this.setState({
        maxBDeg: parseInt(value),
      });
      // Network.sendCommand('get_devpar 25\r\n');//minA
      Network.addComand('get_devpar 25\r\n');
    } else if (param == 25) {
      this.setState({
        minADeg: parseInt(value),
      });
      // Network.sendCommand('get_devpar 26\r\n');//minB
      Network.addComand('get_devpar 26\r\n');
    } else if (param == 26) {
      this.setState({
        minBDeg: parseInt(value),
        loading: false,
      });
    }
  }

  reloadFun = () => {
    // return;
    this.setState({
      reload: !this.state.reload,
    });
  }

  render() {

    if (this.state.loading) {
      return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
        <Spinner
            visible={this.state.loading}
            textContent={'Загрузка...'}
            overlayColor={'rgba(32, 42, 91, 0.3)'}
            textStyle={{color: '#FFF'}}
        />
      </View>);
    } else {
      if (Network.currentDevice && Network.currentDevice.motors == 2) {
        return (<TwoMotorView
          reload={this.state.reload}
          maxADeg={this.state.maxADeg}
          minADeg={this.state.minADeg}
          maxBDeg={this.state.maxBDeg}
          minBDeg={this.state.minBDeg}
          navigation={this.props.navigation}
        />);
      } else {
          return (<OneMotorView
            reload={this.state.reload}
            maxADeg={this.state.maxADeg}
            minADeg={this.state.minADeg}
            maxBDeg={this.state.maxBDeg}
            minBDeg={this.state.minBDeg}
            navigation={this.props.navigation}
          />);
      }
    }
  }
}
