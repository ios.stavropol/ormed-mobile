import React from 'react';
import {
  Dimensions,
  Text,
  View,
  Alert,
  BackHandler,
  TextInput,
  Animated,
  Keyboard,
  Image,
  ImageBackground,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network, { login, getProfile } from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import GradientButton from '../components/Buttons/GradientButton';

export default class LoginScreen extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    headerTitle: (
      <View style={{
        // width: 306,
        flex: 1,
        height: Platform.OS === "android" ? 48 : 44,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
        <Text style={{
          color: 'white',
          fontFamily: 'OpenSans-Semibold',
          fontWeight: '600',
          textAlign: 'center',
          fontSize: Common.getLengthByIPhone7(16),
          lineHeight: Common.getLengthByIPhone7(22),
          letterSpacing: -0.3,
        }}
        allowFontScaling={false}>
            Вход
        </Text>
      </View>
    )
  });

  calculate = true;
  top = new Animated.Value(0);

  state = {
    phone: '',
    phone2: '',
    password: '',
    loading: false,
    buttonY: 0,
    buttonH: 0,
    viewY: 0,
    viewH: 0,
  };

  UNSAFE_componentWillMount() {
    this.props.navigation.addListener('willFocus', this.willFocus);
    this.props.navigation.addListener('willBlur', this.willBlur);
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  willFocus = () => {
    Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    BackHandler.addEventListener(
      "hardwareBackPress",
      () => {
          this.props.navigation.goBack(null);
          return true;
      }
    );
  }

  willBlur = () => {
    // Keyboard.remove('keyboardDidShow');
    // Keyboard.remove('keyboardDidHide');
  }

  _keyboardDidHide = () => {
    this.calculate = false;
    Animated.timing(this.top, {
      toValue: (Dimensions.get('window').height - this.state.viewH)/2,
      duration: 300,
    }).start();
  }

  _keyboardDidShow = e => {
    this.calculate = false;
    let kbView = (Dimensions.get('window').height - e.endCoordinates.height);
    let view = this.state.buttonY+this.state.viewY;
    if (view + Common.getLengthByIPhone7(50) > kbView) {
      Animated.timing(this.top, {
        toValue: (Dimensions.get('window').height - this.state.viewH)/2 - (view + Common.getLengthByIPhone7(50) - kbView),
        duration: 300,
      }).start();
    }
  }

  nextClick = () => {
    this.props.navigation.navigate('Main');
    // if (this.state.phone !== null && this.state.phone.length === 11 && this.state.password !== null && this.state.password.length) {
    //   this.setState({
    //     loading: true,
    //   }, () => {
    //     login(this.state.phone, this.state.password)
    //     .then(response => {
    //       getProfile()
    //       .then(() => {
    //         this.setState({
    //           loading: false,
    //         }, () => {
    //           this.props.navigation.navigate('Main');
    //         });
    //       })
    //       .catch(err => {
    //         this.setState({
    //           loading: false,
    //         }, () => {
    //           setTimeout(() => {
    //             Alert.alert(Config.appName, err);
    //           }, 100);
    //         });
    //       });
    //     })
    //     .catch(err => {
    //       this.setState({
    //         loading: false,
    //       }, () => {
    //         setTimeout(() => {
    //           Alert.alert(Config.appName, err);
    //         }, 100);
    //       });
    //     });
    //   });
    // } else {
    //   Alert.alert(Config.appName, 'Введите номер телефона и пароль!');
    // }
  }

  onLayout = e => {
    if (this.calculate) {
      this.setState({
        buttonH: e.nativeEvent.layout.height,
        buttonY: e.nativeEvent.layout.y,
      })
    }
  }

  onLayout2 = e => {
    if (this.calculate) {
      this.setState({
        viewY: e.nativeEvent.layout.y,
        viewH: e.nativeEvent.layout.height,
      }, () => {
        Animated.timing(this.top, {
          toValue: (Dimensions.get('window').height - this.state.viewH)/2,
          duration: 0,
        }).start();
      })
    }
  }

  render() {

    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        // height: Dimensions.get('window').height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
        <ImageBackground
          source={require('./../assets/ic-background.png')}
          style={{
            resizeMode: 'cover',
            width: Common.getLengthByIPhone7(0),
            height: Dimensions.get('window').height,//Common.getLengthByIPhone7(35),
            marginTop: -44,
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}
        >
          <Animated.View style={{
            alignItems: 'center',
          }}
          onLayout={this.onLayout2}>
            <Image
              source={require('./../assets/ic-logo.png')}
              style={{
                resizeMode: 'contain',
                width: Common.getLengthByIPhone7(159),
                height: Common.getLengthByIPhone7(35),
                marginTop: Common.getLengthByIPhone7(100),
              }}
            />
            <Text style={{
              color: Colors.mainColor,
              fontFamily: 'OpenSans-Bold',
              fontWeight: 'normal',
              textAlign: 'center',
              fontSize: Common.getLengthByIPhone7(22),
              lineHeight: Common.getLengthByIPhone7(30),
              letterSpacing: -0.33,
              marginBottom: Common.getLengthByIPhone7(75),
              marginTop: Common.getLengthByIPhone7(24),
            }}
            allowFontScaling={false}>
                ВАШ ПУТЬ К ЗДОРОВЬЮ
            </Text>
            <View style={{
                width: Common.getLengthByIPhone7(282),
                height: Common.getLengthByIPhone7(44),
                borderRadius: Common.getLengthByIPhone7(10),
                borderWidth: 1,
                borderColor: Colors.grayBorderColor,
                overflow: 'hidden',
              }}>
              <TextInput
                style={{
                  width: Common.getLengthByIPhone7(282),
                  height: Common.getLengthByIPhone7(44),
                  borderRadius: Common.getLengthByIPhone7(10),
                  backgroundColor: 'white',
                  fontFamily: 'OpenSans-Regular',
                  textAlign: 'left',
                  fontWeight: '600',
                  fontSize: Common.getLengthByIPhone7(16),
                  letterSpacing: -0.3,
                  color: Colors.textColor,
                  paddingLeft: Common.getLengthByIPhone7(20),
                }}
                allowFontScaling={false}
                contextMenuHidden={false}
                spellCheck={this.props.spellCheck}
                autoCorrect={this.props.autoCorrect}
                placeholder={'Логин'}
                placeholderTextColor={Colors.placeholderColor}
                autoCompleteType={'off'}
                returnKeyType={'done'}
                inputAccessoryViewID={'myDone'}
                secureTextEntry={false}
                keyboardType={'phone-pad'}
                allowFontScaling={false}
                underlineColorAndroid={'transparent'}
                onSubmitEditing={() => {
                  // this.nextClick();
                  this.passwordInputRef.focus();
                }}
                ref={el => this.textInputRef = el}
                onFocus={() => {

                }}
                onBlur={() => {

                }}
                onChangeText={phone => {
                  let str = phone.split('+');
                  str = str.join('');

                  str = str.split('-');
                  str = str.join('');

                  str = str.split(' ');
                  str = str.join('');

                  str = str.split('(');
                  str = str.join('');

                  str = str.split(')');
                  str = str.join('');

                  let phone2 = Common.reformatPhone(str);

                  if (str.length > 11) {
                    str = str.substring(0, 11);
                  }

                  this.setState({
                    phone: str,
                    phone2: phone2,
                  });
                }}
                value={this.state.phone2}
              />
            </View>
            <View style={{
                width: Common.getLengthByIPhone7(282),
                height: Common.getLengthByIPhone7(44),
                borderRadius: Common.getLengthByIPhone7(10),
                borderWidth: 1,
                borderColor: Colors.grayBorderColor,
                marginTop: Common.getLengthByIPhone7(32),
                overflow: 'hidden',
              }}>
              <TextInput
                style={{
                  width: Common.getLengthByIPhone7(282),
                  height: Common.getLengthByIPhone7(44),
                  borderRadius: Common.getLengthByIPhone7(10),
                  backgroundColor: 'white',
                  fontFamily: 'OpenSans-Regular',
                  textAlign: 'left',
                  fontWeight: '600',
                  fontSize: Common.getLengthByIPhone7(16),
                  letterSpacing: -0.3,
                  color: Colors.textColor,
                  paddingLeft: Common.getLengthByIPhone7(20),
                }}
                allowFontScaling={false}
                contextMenuHidden={false}
                spellCheck={this.props.spellCheck}
                autoCorrect={this.props.autoCorrect}
                placeholder={'Пароль'}
                placeholderTextColor={Colors.placeholderColor}
                autoCompleteType={'off'}
                returnKeyType={'done'}
                inputAccessoryViewID={'myDone'}
                secureTextEntry={true}
                // keyboardType={'phone-pad'}
                allowFontScaling={false}
                underlineColorAndroid={'transparent'}
                onSubmitEditing={() => {
                  this.nextClick();
                }}
                ref={el => this.passwordInputRef = el}
                onFocus={() => {

                }}
                onBlur={() => {

                }}
                onChangeText={password => {
                  this.setState({
                    password,
                  });
                }}
                value={this.state.password}
              />
            </View>
            <GradientButton style={{
              width: Common.getLengthByIPhone7(282),
              height: Common.getLengthByIPhone7(44),
              borderRadius: Common.getLengthByIPhone7(10),
              marginTop: Common.getLengthByIPhone7(32),
            }}
            onLayout={this.onLayout}
            onPress={() => {
              this.nextClick();
            }}
            title={'Вход'}/>
          </Animated.View>
        </ImageBackground>
        <Spinner
          visible={this.state.loading}
          textContent={'Загрузка...'}
          overlayColor={'rgba(32, 42, 91, 0.3)'}
          textStyle={{color: '#FFF'}}
        />
      </View>);
  }
}
