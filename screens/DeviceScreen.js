import React from 'react';
import {
  Platform,
  Text,
  View,
  BackHandler,
  Image,
  NativeModules,
  NativeEventEmitter,
  TouchableOpacity,
  Animated,
  Easing,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
// import { bleManager } from 'react-native-ble-plx';
import BleManager from 'react-native-ble-manager';
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
import { stringToBytes, bytesToString } from 'convert-string';
import Devices from './../constants/Devices';
import OneMotorView from '../components/Device/OneMotorView';
import TwoMotorView from '../components/Device/TwoMotorView';

@observer
export default class DeviceScreen extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
          <View style={{
            // width: 306,
            flex: 1,
            height: Platform.OS === "android" ? 48 : 44,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
            <Text style={{
              color: 'white',
              fontFamily: 'OpenSans-Semibold',
              fontWeight: '600',
              textAlign: 'center',
              fontSize: Common.getLengthByIPhone7(16),
              lineHeight: Common.getLengthByIPhone7(22),
              letterSpacing: -0.3,
            }}
            allowFontScaling={false}>
                {Network.currentDevice.name} s/n{Network.currentDevice.serial}
            </Text>
          </View>
        )
    });

    command = '';
    timer = null;
    directA = new Animated.Value(0);
    directB = new Animated.Value(0);

    state = {
        loading: false,
        error: false,
        reload: false,
        rowIcons: null,
    };

    UNSAFE_componentWillMount() {

        this.setState({
            loading: true,
            error: false,
        });
        this.props.navigation.addListener('willFocus', () => {
        BackHandler.addEventListener(
            "hardwareBackPress",
            () => {
            this.props.navigation.goBack(null);
            return true;
            }
        );
        });

        Network.disconnectDevice = this.disconnectDevice;
        Network.sendCommand = this.sendCommand;
        Network.addComand = this.addComand;

        let data = this.props.navigation.state.params.data;
        Network.selectedDevice = data;

        this.updateValueForCharacteristic = bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', this.handleUpdateValueForCharacteristic );
        // if (data.connected) {
        //     BleManager.disconnect(data.id);
        // }
        setTimeout(() => {
            BleManager.connect(data.id)
            .then(() => {
                Network.selectedDevice.connected = true;
                BleManager.retrieveServices(
                    data.id
                )
                .then(peripheralInfo => {
                    console.warn("retrieveServices: ", peripheralInfo);
                    BleManager.startNotification(
                        data.id,
                        '0000ffe0-0000-1000-8000-00805f9b34fb',
                        '0000ffe1-0000-1000-8000-00805f9b34fb',
                    )
                    .then(() => {
                        this.setState({
                            data,
                        });
                        console.warn('Notification started');
                        setTimeout(() => {
                            this.sendCommand('get_devpar 39\r\n');//type device
                            // Network.addComand('get_devpar 39\r\n');//type device
                        }, 1000);
                    })
                    .catch((error) => {
                        console.warn(error);
                        this.setState({
                            loading: false,
                            error: true,
                        });
                    });
                })
                .catch((error) => {
                    console.warn(error);
                    this.setState({
                        loading: false,
                        error: true,
                    });
                });
            })
            .catch((error) => {
                console.warn('mtu error: ', error);
                this.setState({
                    loading: false,
                    error: true,
                });
            });
        }, 1000);

    }

    UNSAFE_componentWillUnmount() {
        
    }

    constructor(props) {
        super(props);
    }

    toHHMM = secs => {
        if (secs == 0) {
            return '00:00';
        }
        var sec_num = parseInt(secs, 10)
        var hours   = Math.floor(sec_num / 3600)
        var minutes = Math.floor(sec_num / 60) % 60
        hours = hours < 10 ? '0' + hours : hours;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        return hours + ':' + minutes;
    }

    toSS = secs => {
        if (secs == 0) {
            return '00';
        }
        var sec_num = parseInt(secs, 10)
        var seconds = sec_num % 60;
        seconds = seconds < 10 ? '0' + seconds : seconds;
        return seconds;
    }

    startWork = () => {
        this.sendCommand('get_prcsettings\r\n');

        this.timer = setInterval(() => {
            // this.sendCommand('get_prcvals\r\n');
            Network.addComand('get_prcvals\r\n');
        }, 1000);
    }

    addComand = command => {
        Network.queue.push(command);
    }

    handleUpdateValueForCharacteristic = (data, action) => {
        // console.warn('Received data from ' + data.peripheral + ' characteristic ' + data.characteristic, bytesToString(data.value));
        let data2 = bytesToString(data.value);
        if (data2.indexOf('\r\n') === -1) {
            this.command = this.command + data2;
        } else {
            let str = this.command + data2;
            // console.warn('str: ' + str);
            this.command = '';
            
            if (str.indexOf('ok') === 0) {
                if (action) {
                    action();
                }
                // this.sendCommand('get_prcsettings\r\n');
                Network.addComand('get_prcsettings\r\n');
            } else if (str.indexOf('set_devpar') !== -1) {
                console.warn('str: ' + str);
                str = str.split(' ');
                if (str[1] == 39) {
                    let keys = Object.keys(Devices);
                    let device = null;
                    for (let i = 0; i < keys.length; i++) {
                        if (Devices[keys[i]].p39 == str[2]) {
                            device = Devices[keys[i]];
                            Network.currentDevice = device;
                            break;
                        }
                    }

                    if (device) {
                        // this.sendCommand('get_prcsettings\r\n');
                        Network.addComand('get_prcsettings\r\n');
                        this.setState({
                            loading: false,
                            error: false,
                        }, () => {
                            this.startWork();
                        });
                    } else {
                        this.setState({
                            loading: false,
                            error: true,
                        }, () => {
                            this.disconnectDevice();
                        });
                    }
                } else {
                    if (Network.setMinMax) {
                        Network.setMinMax(str[1], str[2]);
                    }
                }
            } else if (str.indexOf('set_prcsettings') !== -1) {
                str = str.split(' ');
                // console.warn('str set_prcsettings: ' + parseInt(str[8]));
                Network.maxDegA = parseInt(str[1]);
                Network.maxDegB = parseInt(str[2]);
                Network.minDegA = parseInt(str[3]);
                Network.minDegB = parseInt(str[4]);
                Network.limitA = parseInt(str[5]);
                Network.limitB = parseInt(str[6]);
                Network.velocity = parseInt(str[7]);
                Network.mode = parseInt(str[8]);

                Network.time = parseInt(str[9]);
                Network.pause = parseInt(str[10]);
                Network.part = parseInt(str[11]);
                Network.pacient = parseInt(str[12]);
                Network.statusA = parseInt(str[13]);
                Network.statusB = parseInt(str[14]);
                Network.nonactiveDegA = parseInt(str[15]);
                Network.nonactiveDegB = parseInt(str[16]);
                Network.loadNegative = parseInt(str[17]);
                Network.loadPositive = parseInt(str[17]);

                this.setState({
                    reload: !this.state.reload,
                });
                if (Network.reloadFun) {
                    Network.reloadFun();
                }
                if (Network.refreshView) {
                    Network.refreshView();
                }
            } else if (str.indexOf('set_prcvals') !== -1) {
                str = str.split(' ');
                // console.warn('str: ' + str);
                Network.nowDegA = parseInt(str[1]);
                Network.nowDegB = parseInt(str[2]);
                Network.nowForce = parseInt(str[3]);
                Network.nowMode = parseInt(str[4]);
                Network.nowTime = parseInt(str[5]);
                Network.nonPrepareA = parseInt(str[6]);
                Network.nonPrepareB = parseInt(str[7]);
                Network.signalA = parseInt(str[8]);
                Network.signalB = parseInt(str[9]);
                Network.nowLoadA = parseInt(str[10]);
                Network.nowLoadB = parseInt(str[11]);
                Network.error = parseInt(str[12]);
                Network.isChanged = parseInt(str[13]);
                Network.inWork = (parseInt(str[4]) & 1);
                Network.inPause = (parseInt(str[4]) & 2) >> 1;
                Network.inStart = (parseInt(str[4]) & 4) >> 2;
                Network.inHome = (parseInt(str[4]) & 16) >> 4;
                Network.inMain = (parseInt(str[4]) & 32) >> 5;
                Network.inDown = (parseInt(str[4]) & 64) >> 6;
                Network.inUp = (parseInt(str[4]) & 128) >> 7;
                Network.inHot = (parseInt(str[4]) & 256) >> 8;
                Network.inTrain = (parseInt(str[4]) & 512) >> 9;
                Network.inComfort = (parseInt(str[4]) & 1024) >> 10;
                Network.inInverse = (parseInt(str[4]) & 2048) >> 11;
                Network.directA = (parseInt(str[4]) & 4096) >> 12;
                Network.inHoldA = (parseInt(str[4]) & 8192) >> 13;
                Network.directB = (parseInt(str[4]) & 16384) >> 14;
                Network.inHoldB = (parseInt(str[4]) & 32768) >> 15;
                Network.hasUsb = (parseInt(str[4]) & 65536) >> 16;
                Network.inActive = (parseInt(str[4]) & 131072) >> 17;
                Network.inHotActive = (parseInt(str[4]) & 262144) >> 18;
                Network.inFreeActive = (parseInt(str[4]) & 524288) >> 19;
                // console.warn('inWork: '+Network.inWork+' inPause: '+Network.inPause+' inStart: '+Network.inStart+' inHome: '+Network.inHome);
                // console.warn('str[4]: ' + str[4] +' A: ' + Network.directA + ' B: ' + Network.directB);

                
                if (Network.reloadFun) {
                    Network.reloadFun();
                }
                if (Network.refreshView) {
                    Network.refreshView();
                }
                this.setState({
                    reload: !this.state.reload,
                });
            }
            //execute
            if (Network.queue.length) {
                Network.queue.shift();
                if (Network.queue.length) {
                    this.sendCommand(Network.queue[0]);
                } else {
                    Network.addComand('get_prcvals\r\n');
                    Network.sendCommand('get_prcvals\r\n');
                }
            } else {
                Network.addComand('get_prcvals\r\n');
                Network.sendCommand('get_prcvals\r\n');
            }
        }
    }

    sendCommand = command => {
        // let str = 'raise_ev 5\r\n';
        if (Network.selectedDevice) {
            BleManager.write(
                Network.selectedDevice.id,
                '0000ffe0-0000-1000-8000-00805f9b34fb',
                '0000ffe1-0000-1000-8000-00805f9b34fb',
                stringToBytes(command),
                stringToBytes(command).length
            )
            .then(() => { 
                // console.warn('write data: ', stringToBytes(command));
            })
            .catch((error) => {
                console.warn('sendCommand: ', error);
                setTimeout(() => {
                    // this.sendCommand(command);
                    Network.addComand(command);
                }, 1000);
            });
        }
    }

    disconnectDevice = () => {
        this.setState({
            loading: false,
            error: true,
        });
        this.updateValueForCharacteristic.remove();
        clearInterval(this.timer);
        BleManager.stopNotification(Network.selectedDevice.id, '0000ffe0-0000-1000-8000-00805f9b34fb', '0000ffe1-0000-1000-8000-00805f9b34fb',)
        .then(() => {

        })
        .catch(err => {
            
        });
        if (Network.selectedDevice !== null && Network.selectedDevice.connected) {
            BleManager.disconnect(Network.selectedDevice.id)
            .then(() => {
                console.warn("Disconnected");
            })
            .catch((error) => {
                console.warn(error);
            });
            Network.selectedDevice = null;
        }
    }

    renderError = () => {
        return (<View style={{
            flex: 1,
            width: Common.getLengthByIPhone7(0),
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'white',
        }}>
            <Text style={{
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                color: Colors.mainColor,
                fontFamily: 'OpenSans-Bold',
                fontWeight: 'normal',
                textAlign: 'center',
                fontSize: Common.getLengthByIPhone7(20),
            }}
            allowFontScaling={false}>
                Связаться с устройством не удалось!
            </Text>
        </View>);
    }

    render() {

        let body = null;
        if (this.state.loading) {
            body = null;
        } else if (this.state.error) {
            body = this.renderError();
        } else {
            if (Network.currentDevice && Network.currentDevice.motors == 2) {
                body = (<TwoMotorView
                    reload={this.state.reload}
                    navigation={this.props.navigation}
                />);
            } else {
                body = (<OneMotorView
                    reload={this.state.reload}
                    navigation={this.props.navigation}
                />);
            }
        }

        return (<View style={{
            flex: 1,
            width: Common.getLengthByIPhone7(0),
            // height: Dimensions.get('window').height,
            justifyContent: 'flex-start',
            alignItems: 'center',
            backgroundColor: 'white',
        }}>
            {body}
            <Spinner
                visible={this.state.loading}
                textContent={'Загрузка...'}
                overlayColor={'rgba(32, 42, 91, 0.3)'}
                textStyle={{color: '#FFF'}}
            />
        </View>);
    }
}
