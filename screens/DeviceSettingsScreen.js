import React from 'react';
import {
  Platform,
  Text,
  View,
  BackHandler,
  Alert,
  RefreshControl,
  Linking,
  Image,
  NativeModules,
  NativeEventEmitter,
  PermissionsAndroid,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';

@observer
export default class DeviceSettingsScreen extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    headerTitle: (
      <View style={{
        // width: 306,
        flex: 1,
        height: Platform.OS === "android" ? 48 : 44,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
        <Text style={{
          color: 'white',
          fontFamily: 'OpenSans-Semibold',
          fontWeight: '600',
          textAlign: 'center',
          fontSize: Common.getLengthByIPhone7(16),
          lineHeight: Common.getLengthByIPhone7(22),
          letterSpacing: -0.3,
        }}
        allowFontScaling={false}>
            {Network.currentDevice.name} s/n{Network.currentDevice.serial}
        </Text>
      </View>
    )
  });

  state = {
    loading: false,
  };

  UNSAFE_componentWillMount() {

    this.props.navigation.addListener('willFocus', () => {
      BackHandler.addEventListener(
        "hardwareBackPress",
        () => {
          this.props.navigation.goBack(null);
          return true;
        }
      );
    });
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);

  }

  render() {
    return (<View style={{
      flex: 1,
      width: Common.getLengthByIPhone7(0),
      // height: Dimensions.get('window').height,
      justifyContent: 'flex-start',
      alignItems: 'center',
      backgroundColor: 'white',
    }}>
        
    </View>);
  }
}
