import React from 'react';
import {
  Dimensions,
  Text,
  View,
  Alert,
  BackHandler,
  Image,
  Animated,
  Keyboard,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';

export default class ProfileScreen extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    headerTitle: (
      <View style={{
        // width: 306,
        flex: 1,
        height: Platform.OS === "android" ? 48 : 44,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
        <Text style={{
          color: 'white',
          fontFamily: 'OpenSans-Semibold',
          fontWeight: '600',
          textAlign: 'center',
          fontSize: Common.getLengthByIPhone7(16),
          lineHeight: Common.getLengthByIPhone7(22),
          letterSpacing: -0.3,
        }}
        allowFontScaling={false}>
            Профиль
        </Text>
      </View>
    )
  });

  state = {
    loading: false,
  };

  UNSAFE_componentWillMount() {

    BackHandler.addEventListener(
      "hardwareBackPress",
      () => {
          this.props.navigation.goBack(null);
          return true;
      }
    );
    this.props.navigation.addListener('willFocus', this.willFocus);
    this.props.navigation.addListener('willBlur', this.willBlur);
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  willFocus = () => {
    // Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    // Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  willBlur = () => {
    // Keyboard.removeListener('keyboardDidShow');
    // Keyboard.removeListener('keyboardDidHide');
  }

  render() {

    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        // height: Dimensions.get('window').height,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
        <Spinner
            visible={this.state.loading}
            textContent={'Загрузка...'}
            overlayColor={'rgba(32, 42, 91, 0.3)'}
            textStyle={{color: '#FFF'}}
        />
      </View>);
  }
}
