import React from 'react';
import {
  Platform,
  View,
  Image,
  Dimensions,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network, {getProfile} from './../Utilites/Network';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SplashScreen from 'react-native-splash-screen';

export default class SplashScreen2 extends React.Component {

  state = {

  };

  UNSAFE_componentWillMount() {
    AsyncStorage.getItem('info')
    .then(token => {
      if (token !== null && token.length) {
    //     Network.access_token = token;
    //     getProfile()
    //     .then(() => {
          // this.props.navigation.navigate('Roles');
          this.props.navigation.navigate('Main');
          setTimeout(() => {
            SplashScreen.hide();
          }, 3000);
    //     })
    //     .catch(err => {
    //       this.props.navigation.navigate('Login');
    //     });
      } else {
        this.props.navigation.navigate('Info');
        setTimeout(() => {
          SplashScreen.hide();
        }, 3000);
      }
    })
    .catch(err => {
      this.props.navigation.navigate('Info');
      setTimeout(() => {
        SplashScreen.hide();
      }, 3000);
    });
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);
  }

  render() {

    return (<View style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#EAF2FF',
      }}>
        <Image source={require('./../assets/splash.png')} style={{
          resizeMode: 'cover',
          width: Common.getLengthByIPhone7(0),
          height: Dimensions.get('window').height,
        }} />
      </View>);
  }
}
