import React from 'react';
import {
  Platform,
  Text,
  View,
  BackHandler,
  Alert,
  ImageBackground,
  FlatList,
  Dimensions,
  NativeModules,
  NativeEventEmitter,
  PermissionsAndroid,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network from './../Utilites/Network';
import DeviceView from '../components/Main/DeviceView';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
import BleManager from 'react-native-ble-manager';
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

@observer
export default class MainScreen extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    headerTitle: (
      <View style={{
        // width: 306,
        flex: 1,
        height: Platform.OS === "android" ? 48 : 44,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
        <Text style={{
          color: 'white',
          fontFamily: 'OpenSans-Semibold',
          fontWeight: '600',
          textAlign: 'center',
          fontSize: Common.getLengthByIPhone7(16),
          lineHeight: Common.getLengthByIPhone7(22),
          letterSpacing: -0.3,
        }}
        allowFontScaling={false}>
            Список устройств
        </Text>
      </View>
    )
  });

  state = {
    latitude: 0,
    longitude: 0,
    loading: false,
    refresh: false,
    showOrder: false,
    order: null,
    devices: {},
    connectedPerf: null,
  };

  UNSAFE_componentWillMount() {

    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then((result) => {
          if (result) {
            console.warn("Permission is OK");
          } else {
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then((result) => {
              if (result) {
                console.warn("User accept");
              } else {
                console.warn("User refuse");
              }
            });
          }
      });
    }

    BleManager.start({
      showAlert: false,
      forceLegacy: true,
    }).then(() => {
      // Success code
      console.warn("Module initialized");
      BleManager.scan([], 0, true, {
        // matchMode: 1,//MATCH_MODE_AGGRESSIVE
        // matchMode: 2,//MATCH_MODE_STICKY
        // numberOfMatches: 3,
        // scanMode: 
      }).then(() => {
        // Success code
        console.warn("Scan started");
      });
      
      this.discoverPeripheral = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral);
      this.stopScan = bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan );
      this.disconnectedPeripheral = bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', this.handleDisconnectedPeripheral );
      // this.updateValueForCharacteristic = bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', this.handleUpdateValueForCharacteristic );
      this.didUpdateState = bleManagerEmitter.addListener("BleManagerDidUpdateState", (args) => {
        console.warn("BleManagerDidUpdateState: " + JSON.stringify(args));
        if (args.state === 'off') {
          BleManager.stopScan().then(() => {
            // Success code
            console.warn("Scan stopped");
          });
        } else if (args.state === 'on') {
          BleManager.scan([], 0, true).then(() => {
            // Success code
            console.warn("Scan started");
          });
        }
      })
    });

    this.props.navigation.addListener('willFocus', () => {
      if (Network.disconnectDevice) {
        Network.disconnectDevice();
      }
      BackHandler.addEventListener(
        "hardwareBackPress",
        () => {
          BackHandler.exitApp()
          return true;
        }
      );
    });
  }

  UNSAFE_componentWillUnmount() {
    if (this.state.connectedPerf !== null) {
      BleManager.disconnect(this.state.connectedPerf)
      .then(() => {
        console.warn("Disconnected");
      })
      .catch((error) => {
        console.warn(error);
      });
    }

    this.discoverPeripheral.remove();
    this.stopScan.remove();
    this.disconnectedPeripheral.remove();
    // this.updateValueForCharacteristic.remove();
  }

  constructor(props) {
    super(props);

  }

  handleStopScan = () => {
    console.warn('handleStopScan');
    // setIsScanning(false);
  }

  handleDisconnectedPeripheral = (data) => {
    // let peripheral = peripherals.get(data.peripheral);
    // if (peripheral) {
      // peripheral.connected = false;
      // peripherals.set(peripheral.id, peripheral);
      // setList(Array.from(peripherals.values()));
    // }
    console.warn('Disconnected from ' + data.peripheral);
    this.setState({
      connectedPerf: null,
    });
  }

  // handleUpdateValueForCharacteristic = (data) => {
  //   console.warn('Received data from ' + data.peripheral + ' characteristic ' + data.characteristic, bytesToString(data.value));
  // }

  handleDiscoverPeripheral = (peripheral) => {
    // console.warn('Got ble peripheral', peripheral);
    if (!peripheral.name) {
      peripheral.name = 'NO NAME';
    }
    if (peripheral.name.indexOf('Flex') === 0) {
      if (peripheral.name.length) {
      peripheral.time = new Date();
      // peripherals.set(peripheral.id, peripheral);
      // setList(Array.from(peripherals.values()));
      let devices = this.state.devices;

      if (!devices[peripheral.id]) {
        devices[peripheral.id] = peripheral;

        // let keys = Object.keys(devices);

        // for (let i = 0; i < keys.length; i++) {
        //   if ((new Date()).getTime() - devices[keys[i]].time.getTime() > 1000*60) {
        //     delete devices[keys[i]];
        //   }
        // }
        this.setState({
          devices
        }, () => {
          // console.warn('devices: ' +  JSON.stringify(this.state.devices));
        });
      }
    }
  }
  }

  render() {

    let rows = [];

    let keys = Object.keys(this.state.devices);
    for (let i = 0; i < keys.length; i++) {
      rows.push(<DeviceView
        style={{
          marginTop: i === 0 ? Common.getLengthByIPhone7(51) : 0,
        }}
        navigation={this.props.navigation}
        index={i}
        data={this.state.devices[keys[i]]}
      />);
    }

    return (<View style={{
      flex: 1,
      width: Common.getLengthByIPhone7(0),
      // height: Dimensions.get('window').height,
      justifyContent: 'flex-start',
      alignItems: 'center',
      backgroundColor: 'white',
    }}>
      <ImageBackground
        source={require('./../assets/ic-background.png')}
        style={{
          resizeMode: 'cover',
          width: Common.getLengthByIPhone7(0),
          height: Dimensions.get('window').height,//Common.getLengthByIPhone7(35),
          marginTop: -44,
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <ScrollView
          style={{
            flex: 1,
            width: Common.getLengthByIPhone7(0),
            marginTop: 44, //Common.getLengthByIPhone7(50),
            marginBottom: 30,
          }}
          contentContainerStyle={{
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}
        >
          {rows}
        </ScrollView>
      </ImageBackground>
      <Spinner
        visible={this.state.loading}
        textContent={'Загрузка...'}
        overlayColor={'rgba(32, 42, 91, 0.3)'}
        textStyle={{color: '#FFF'}}
      />
    </View>);
  }
}
