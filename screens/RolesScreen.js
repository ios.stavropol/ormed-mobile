import React from 'react';
import {
  Dimensions,
  Text,
  View,
  Alert,
  TouchableOpacity,
  Image,
  Animated,
  Keyboard,
  BackHandler,
  ImageBackground,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import RoleButton from '../components/Roles/RoleButton';

export default class RolesScreen extends React.Component {

    calculate = true;
    top = new Animated.Value(0);

    state = {
        code: '',
        loading: false,
        buttonY: 0,
        buttonH: 0,
        viewY: 0,
        viewH: 0,
    };

  UNSAFE_componentWillMount() {
    this.props.navigation.addListener('willFocus', this.willFocus);
    this.props.navigation.addListener('willBlur', this.willBlur);
  }

  UNSAFE_componentWillUnmount() {}

  constructor(props) {
    super(props);
  }

  willFocus = () => {
    Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    BackHandler.addEventListener(
        "hardwareBackPress",
        () => {
            // this.props.navigation.goBack(null);
            return true;
        }
    );
  }

  willBlur = () => {
    // Keyboard.removeListener('keyboardDidShow');
    // Keyboard.removeListener('keyboardDidHide');
  }

  renderButton = (title, icon, action) => {
    return (<View style={{

    }}>
      <TouchableOpacity style={{
        width: Common.getLengthByIPhone7(84),
        height: Common.getLengthByIPhone7(78),
        borderRadius: Common.getLengthByIPhone7(10),
        backgroundColor: 'white',
        // shadowColor: '#D7DEEC',
        // shadowOffset: { width: 0, height: Common.getLengthByIPhone7(5) },
        // shadowOpacity: 1,
        // shadowRadius: Common.getLengthByIPhone7(10),
        elevation: 4,
        alignItems: 'center',
        justifyContent: 'center'
      }}
      onPress={() => {
          if (action) {
            action();
          }
      }}>
        <Image
          source={icon}
          style={{
              resizeMode: 'contain',
              width: Common.getLengthByIPhone7(46),
              height: Common.getLengthByIPhone7(46),
          }}
        />
      </TouchableOpacity>
      <Text style={{
        color: Colors.textColor,
        fontFamily: 'OpenSans-SemiBold',
        fontWeight: '600',
        textAlign: 'center',
        fontSize: Common.getLengthByIPhone7(18),
        lineHeight: Common.getLengthByIPhone7(25),
        marginTop: Common.getLengthByIPhone7(17),
      }}
      allowFontScaling={false}>
          {title}
      </Text>
    </View>);
  }

  render() {

    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
        <ImageBackground
          source={require('./../assets/ic-background.png')}
          style={{
            resizeMode: 'cover',
            width: Common.getLengthByIPhone7(0),
            // height: Common.getLengthByIPhone7(35),
            flex: 1,
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}
        >
          <Image
            source={require('./../assets/ic-logo.png')}
            style={{
              resizeMode: 'contain',
              width: Common.getLengthByIPhone7(159),
              height: Common.getLengthByIPhone7(35),
              marginTop: Common.getLengthByIPhone7(30),
            }}
          />
          <Text style={{
            color: Colors.mainColor,
            fontFamily: 'OpenSans-Bold',
            fontWeight: 'normal',
            textAlign: 'center',
            fontSize: Common.getLengthByIPhone7(22),
            lineHeight: Common.getLengthByIPhone7(30),
            letterSpacing: -0.33,
            marginBottom: Common.getLengthByIPhone7(92),
            marginTop: Common.getLengthByIPhone7(24),
          }}
          allowFontScaling={false}>
              ВАШ ПУТЬ К ЗДОРОВЬЮ
          </Text>
          <Text style={{
            color: Colors.textColor,
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: 'normal',
            textAlign: 'center',
            fontSize: Common.getLengthByIPhone7(22),
            lineHeight: Common.getLengthByIPhone7(30),
            letterSpacing: -0.33,
            marginBottom: Common.getLengthByIPhone7(35),
          }}
          allowFontScaling={false}>
              Выберите категорию
          </Text>
          <View style={{
            width: Common.getLengthByIPhone7(322),
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: 'white',
          }}>
            <RoleButton
              navigation={this.props.navigation}
              title={'Аноним'}
              activeIcon={require('./../assets/ic-role-anonim-act.png')}
              inactiveIcon={require('./../assets/ic-role-anonim.png')}
              action={() => {
                this.props.navigation.navigate('Main');
              }}
            />
            <RoleButton
              navigation={this.props.navigation}
              title={'Пациент'}
              activeIcon={require('./../assets/ic-role-pacient-act.png')}
              inactiveIcon={require('./../assets/ic-role-pacient.png')}
              action={() => {
                this.props.navigation.navigate('Login');
              }}
            />
            <RoleButton
              navigation={this.props.navigation}
              title={'Врач'}
              activeIcon={require('./../assets/ic-role-doctor-act.png')}
              inactiveIcon={require('./../assets/ic-role-doctor.png')}
              action={() => {
                this.props.navigation.navigate('Login');
              }}
            />
          </View>
        </ImageBackground>
      </View>);
  }
}
