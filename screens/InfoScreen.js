import React from 'react';
import {
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class InfoScreen extends React.Component {

  state = {
    
  };

  UNSAFE_componentWillMount() {

    
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);

  }

  render() {

    
    return (<View style={{
      flex: 1,
      width: Common.getLengthByIPhone7(0),
      // height: Dimensions.get('window').height,
      justifyContent: 'flex-start',
      alignItems: 'center',
      backgroundColor: 'white',
    }}>
      <ImageBackground
        source={require('./../assets/ic-background.png')}
        style={{
          resizeMode: 'cover',
          width: Common.getLengthByIPhone7(0),
          height: Dimensions.get('window').height,//Common.getLengthByIPhone7(35),
          marginTop: -44,
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Text style={{
			width: Common.getLengthByIPhone7(300),
			color: 'black',
			fontFamily: 'OpenSans-Semibold',
			fontWeight: '600',
			textAlign: 'center',
			fontSize: Common.getLengthByIPhone7(18),
			lineHeight: Common.getLengthByIPhone7(22),
			letterSpacing: -0.3,
        }}
        allowFontScaling={false}>
            {Config.appName} {`использует доступ к Bluetooth и геолокации\nдля корректной работы с аппаратами Flex.\nДанные никуда не передаются.`}
        </Text>
		<TouchableOpacity style={{
			width: Common.getLengthByIPhone7(200),
			height: Common.getLengthByIPhone7(40),
			borderRadius: Common.getLengthByIPhone7(10),
			backgroundColor: Colors.mainColor,
			alignItems: 'center',
			justifyContent: 'center',
			marginTop: Common.getLengthByIPhone7(20),
		}}
		onPress={() => {
			AsyncStorage.setItem('info', '111');
			this.props.navigation.navigate('Main');
		}}>
			<Text style={{
				color: 'white',
				fontFamily: 'OpenSans-Semibold',
				fontWeight: '600',
				textAlign: 'center',
				fontSize: Common.getLengthByIPhone7(18),
				lineHeight: Common.getLengthByIPhone7(22),
				letterSpacing: -0.3,
			}}
			allowFontScaling={false}>
				Далее
			</Text>
		</TouchableOpacity>
      </ImageBackground>
    </View>);
  }
}
