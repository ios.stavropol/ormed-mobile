import React from 'react';
import {
  Platform,
  Text,
  View,
  BackHandler,
  Alert,
  RefreshControl,
  Linking,
  Image,
  NativeModules,
  NativeEventEmitter,
  PermissionsAndroid,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
import Button from '../components/Help/Button';

@observer
export default class HelpScreen extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    headerTitle: (
      <View style={{
        // width: 306,
        flex: 1,
        height: Platform.OS === "android" ? 48 : 44,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
        <Text style={{
          color: 'white',
          fontFamily: 'OpenSans-Semibold',
          fontWeight: '600',
          textAlign: 'center',
          fontSize: Common.getLengthByIPhone7(16),
          lineHeight: Common.getLengthByIPhone7(22),
          letterSpacing: -0.3,
        }}
        allowFontScaling={false}>
            {Network.currentDevice.name} s/n{Network.currentDevice.serial}
        </Text>
      </View>
    )
  });

  state = {
    loading: false,
  };

  UNSAFE_componentWillMount() {

    this.props.navigation.addListener('willFocus', () => {
      BackHandler.addEventListener(
        "hardwareBackPress",
        () => {
          this.props.navigation.goBack(null);
          return true;
        }
      );
    });
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);

  }

  renderButton = (name, icon, style, action) => {
      return (<TouchableOpacity style={[{
        width: Common.getLengthByIPhone7(319),
        height: Common.getLengthByIPhone7(89),
        borderRadius: Common.getLengthByIPhone7(10),
        backgroundColor: 'white',
        shadowColor: '#D7DEEC',
        shadowOffset: { width: 0, height: Common.getLengthByIPhone7(5) },
        shadowOpacity: 1,
        shadowRadius: Common.getLengthByIPhone7(10),
        elevation: 4,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
      }, style]}
      onPress={() => {
          if (action) {
            action();
          }
      }}>
        <Image
          source={icon}
          style={{
            resizeMode: 'contain',
            width: Common.getLengthByIPhone7(51),
            height: Common.getLengthByIPhone7(51),
            marginLeft: Common.getLengthByIPhone7(17),
          }}
        />
        <Text style={{
          color: Colors.mainColor,
          fontFamily: 'OpenSans-Bold',
          fontWeight: 'bold',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(18),
          marginLeft: Common.getLengthByIPhone7(35),
        }}
        allowFontScaling={false}>
            {name}
        </Text>
      </TouchableOpacity>);
  }

  render() {
    return (<View style={{
      flex: 1,
      width: Common.getLengthByIPhone7(0),
      // height: Dimensions.get('window').height,
      justifyContent: 'flex-start',
      alignItems: 'center',
      backgroundColor: 'white',
    }}>
      <ImageBackground
        source={require('./../assets/ic-background.png')}
        style={{
          resizeMode: 'cover',
          width: Common.getLengthByIPhone7(0),
          // height: Common.getLengthByIPhone7(35),
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Button
          title={'Техподдержка'}
          inactiveIcon={require('./../assets/ic-support.png')}
          activeIcon={require('./../assets/ic-support-act.png')}
          style={{
            //marginTop: Common.getLengthByIPhone7(16),
          }}
          action={() => {
            let url = 'tel:89374920770';
            Linking.canOpenURL(url).then(supported => {
              if (supported) {
                Linking.openURL(url);
              } else {
                console.log("Don't know how to open URI: " + url);
              } 
            });
          }
        }/>
        <Button
          title={'Инструкция'}
          inactiveIcon={require('./../assets/ic-instruction.png')}
          activeIcon={require('./../assets/ic-instruction-act.png')}
          style={{
            marginTop: Common.getLengthByIPhone7(24),
          }}
          action={() => {
            let url = 'https://docs.google.com/viewer?url=' + Network.currentDevice.doc;//'http://api.mypubs.ru/uploads/F04.pdf'; 
            Linking.canOpenURL(url).then(supported => {
              if (supported) {
                Linking.openURL(url);
              } else {
                console.log("Don't know how to open URI: " + url);
              }
            });
          }
        }/>
        <Button
          title={'Видео'}
          inactiveIcon={require('./../assets/ic-video.png')}
          activeIcon={require('./../assets/ic-video.png')}
          style={{
            marginTop: Common.getLengthByIPhone7(24),
          }}
          action={() => {
            let url = Network.currentDevice.video;
            Linking.canOpenURL(url).then(supported => {
              if (supported) {
                Linking.openURL(url);
              } else {
                console.log("Don't know how to open URI: " + url);
              }
            });
          }
        }/>
        <Button
          title={'Сайт'}
          inactiveIcon={require('./../assets/ic-site.png')}
          activeIcon={require('./../assets/ic-site-act.png')}
          style={{
            marginTop: Common.getLengthByIPhone7(24),
          }}
          action={() => {
            let url = 'https://www.ormed.ru';
            Linking.canOpenURL(url).then(supported => {
              if (supported) {
                Linking.openURL(url);
              } else {
                console.log("Don't know how to open URI: " + url);
              }
            });
          }
        }/>
      </ImageBackground>
    </View>);
  }
}
