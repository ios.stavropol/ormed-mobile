const Colors = {
  yellowColor: '#FFC51B',
  textColor: '#333333',
  grayC4: 'rgba(196, 196, 196, 0.38)',
  grayC1: '#c1c1c1',
  mainColor: '#4D94FE',
  placeholderColor: '#CDC8C8',
  grayBorderColor: '#CED1E0',
  disabledColor: '#D3D3D3',


  textGrayColor: '#5A5E75',
  blueTextColor: '#7D819A',
  violetColor: '#5E4BAD',
  redColor: '#EA5055',
  greenColor: '#1EBA17',
  fonColor: '#F3F6FC',
  shadowColor: '#D7DEEC',
  lineColor: '#E5E7F1',
};

export default Colors;