const Config = {
  apiDomain:              'https://imoving.kz',
  version:                '1.0',
  appName:                'ORMED mobile',
};

export default Config;
