import React from 'react';
import {
  Text,
  View,
  Image,
  Linking,
  TouchableOpacity,
  Alert,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network, { changeStatus } from './../../Utilites/Network';
import Modal from 'react-native-modal';
import GradientButton from '../Buttons/GradientButton';
import AnimatedTextInputView from './../AnimatedTextInputView';

export default class ChangePasswordModalView extends React.Component {

  state = {
    show: false,
    password: '',
    password_confirm: '',
  };

  UNSAFE_componentWillMount() {
    this.setState({
        show: this.props.show,
        password: '',
        password_confirm: '',
    });
  }

  UNSAFE_componentWillUnmount() {}

  UNSAFE_componentWillReceiveProps(props) {
      if (props.show !== this.state.show) {
        this.setState({
            show: props.show,
            password: '',
            password_confirm: '',
        });
      } else {
        // this.setState({
        //     show: props.show,
        //   //   password: '',
        //   //     password_confirm: '',
        // });
      }
  }

  constructor(props) {
    super(props);

  }

  render() {

    return (
    <Modal
        isVisible={this.state.show}
        animationIn={'fadeIn'}
        animationOut={'fadeOut'}
    >
        <View style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
        }}>
            <View style={{
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                borderRadius: Common.getLengthByIPhone7(10),
                overflow: 'hidden',
                backgroundColor: 'white',
                alignItems: 'center',
                justifyContent: 'flex-start',
                padding: Common.getLengthByIPhone7(16),
            }}>
                <Text style={{
                    fontFamily: 'Arial',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(20),
                    fontWeight: 'bold',
                    color: 'black',
                    marginBottom: Common.getLengthByIPhone7(10),
                }}>
                    Изменить пароль
                </Text>
                <View style={{
                    width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
                    overflow: 'hidden',
                }}>
                    <AnimatedTextInputView
                        style={{
                            // marginTop: Common.getLengthByIPhone7(10),
                            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
                        }}
                        secureTextEntry={true}
                        value={this.state.password}
                        title={'Пароль'}
                        onChangeText={password => {
                            this.setState({
                                password,
                            });
                        }}
                    />
                </View>
                <View style={{
                    // marginTop: Common.getLengthByIPhone7(10),
                    width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
                    overflow: 'hidden',
                }}>
                    <AnimatedTextInputView
                        style={{
                            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
                        }}
                        secureTextEntry={true}
                        value={this.state.password_confirm}
                        title={'Еще пароль'}
                        onChangeText={password_confirm => {
                            this.setState({
                                password_confirm,
                            });
                        }}
                    />
                </View>
                <View style={{
                    width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
                    marginTop: Common.getLengthByIPhone7(20),
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                }}>
                    <GradientButton style={{
                        width: Common.getLengthByIPhone7(130),
                        height: Common.getLengthByIPhone7(40),
                        borderRadius: Common.getLengthByIPhone7(4),
                    }}
                    onPress={() => {
                        if (this.props.onClose) {
                            this.props.onClose();
                        }
                    }}
                    title={'Отмена'}/>
                    <GradientButton style={{
                        width: Common.getLengthByIPhone7(130),
                        height: Common.getLengthByIPhone7(40),
                        borderRadius: Common.getLengthByIPhone7(4),
                    }}
                    onPress={() => {
                        if (this.state.password.length && this.state.password_confirm.length) {
                            if (this.state.password === this.state.password_confirm) {
                                if (this.props.onChange) {
                                    this.props.onChange(this.state.password, this.state.password_confirm);
                                }
                            } else {
                                Alert.alert(Config.appName, 'Пароли должны совпадать!');
                            }
                        } else {
                            Alert.alert(Config.appName, 'Введите пароль!');
                        }
                    }}
                    title={'Изменить'}/>
                </View>
            </View>
        </View>
    </Modal>);
  }
}
