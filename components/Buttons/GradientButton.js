import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import Colors from './../../constants/Colors';
import Common from './../../Utilites/Common';
import LinearGradient from 'react-native-linear-gradient';

export default class GradientButton extends React.Component {

  state = {
    
  };

  UNSAFE_componentWillMount() {
    
  }

  UNSAFE_componentWillUnmount() {

  }

  UNSAFE_componentWillReceiveProps(props) {
    
  }

  constructor(props) {
    super(props);

  }

  render() {
    
    return (<TouchableOpacity style={[{
      backgroundColor: Colors.mainColor,
      alignItems: 'center',
      justifyContent: 'center',
    }, this.props.style]}
    onLayout={e => {
      if (this.props.onLayout) {
        this.props.onLayout(e);
      }
    }}
    onPress={() => {
      if (this.props.onPress) {
        this.props.onPress();
      }
    }}>
      <Text style={{
          color: 'white',
          fontFamily: 'Rubik-Medium',
          fontWeight: 'normal',
          textAlign: 'center',
          fontSize: Common.getLengthByIPhone7(18),
      }}
      allowFontScaling={false}>
        {this.props.title}
      </Text>
    </TouchableOpacity>);
  }

//   <LinearGradient colors={[Colors.yellowColor, '#F99131']} style={{
//     position: 'absolute',
//     left: 0,
//     top: 0,
//     right: 0,
//     bottom: 0,
//     borderRadius: Common.getLengthByIPhone7(15),
//     alignItems: 'center',
//     justifyContent: 'center',
// }}
// start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}>
}
