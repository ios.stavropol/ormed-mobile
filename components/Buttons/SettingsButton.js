import React from "react";
import { View, Image, Dimensions, TouchableWithoutFeedback, Alert } from "react-native";
import { NavigationActions } from "react-navigation";
import Common from './../../Utilites/Common'
import Network from './../../Utilites/Network'
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import { Header } from 'react-navigation-stack';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class SettingsButton extends React.Component {
    goToSettings = () => {
        this.props.navigation.navigate('DeviceSettings');
    };

    render() {

      let scale = Header.HEIGHT/88;
      scale = 1;
      return (
          <View style={{
            backgroundColor: 'transparent',
            flex: 0,
          }}>
              <TouchableWithoutFeedback onPress={this.goToSettings}>
                <View style={{
                  width: Common.getLengthByIPhone7(45),
                  height: Common.getLengthByIPhone7(45),
                  alignItems: 'flex-start',
                  justifyContent: 'center',
                }}>
                  <Image source={require('./../../assets/ic-setting.png')} style={{
                    resizeMode: 'contain',
                    width: Common.getLengthByIPhone7(27),
                    height: Common.getLengthByIPhone7(27),
                    tintColor: 'white',
                  }} />
                </View>
              </TouchableWithoutFeedback>
          </View>
      )
    }
}
