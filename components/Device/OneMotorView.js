import React from 'react';
import {
  Platform,
  Text,
  View,
  BackHandler,
  Image,
  NativeModules,
  NativeEventEmitter,
  TouchableOpacity,
  Animated,
  Easing,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
// import { bleManager } from 'react-native-ble-plx';
import BleManager from 'react-native-ble-manager';
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
import { stringToBytes, bytesToString } from 'convert-string';
import StartStopButton from './StartStopButton';
import network from './../../Utilites/Network';
import Devices from './../../constants/Devices';

@observer
export default class OneMotorView extends React.Component {

	command = '';
	timer = null;
	directA = new Animated.Value(0);

	state = {
		loading: false,
		reload: false,
		rowIcons: null,
	};

	UNSAFE_componentWillMount() {
		Network.refreshView = this.refreshView;
	}

	UNSAFE_componentWillReceiveProps(props) {
		this.setState({
			reload: props.reload,
		});
	}

	UNSAFE_componentWillUnmount() {
		
	}

	constructor(props) {
		super(props);
	}

	refreshView = () => {
		Animated.timing(this.directA,
			{
				toValue: Network.directA,
				duration: 30,
				easing: Easing.linear,
				useNativeDriver: true
			}
		).start()
		this.renderIcons();
	}
    toHHMM = secs => {
        if (secs == 0) {
            return '00:00';
        }
        var sec_num = parseInt(secs, 10)
        var hours   = Math.floor(sec_num / 3600)
        var minutes = Math.floor(sec_num / 60) % 60
        hours = hours < 10 ? '0' + hours : hours;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        return hours + ':' + minutes;
    }

    toSS = secs => {
        if (secs == 0) {
            return '00';
        }
        var sec_num = parseInt(secs, 10)
        var seconds = sec_num % 60;
        seconds = seconds < 10 ? '0' + seconds : seconds;
        return seconds;
    }

  
	renderIcon = (icon, style) => {
		return (<View style={[{
			width: Common.getLengthByIPhone7(44),
			height: Common.getLengthByIPhone7(44),
			alignItems: 'center',
			justifyContent: 'center',
		}, style]}>
			<Image
				source={icon}
				style={{
					resizeMode: 'contain',
					width: Common.getLengthByIPhone7(36),
					height: Common.getLengthByIPhone7(36),
					tintColor: Colors.mainColor,
				}}
			/>
		</View>);
	}

	renderIcons = () => {
		let rows = [];
		// let upper = (parseInt(Network.mode) & 1);
		// let down = (parseInt(Network.mode) & 2) >> 1;
		// let hot = (parseInt(Network.mode) & 4) >> 2;
		// let comfort = (parseInt(Network.mode) & 8) >> 3;
		// let train = (parseInt(Network.mode) & 16) >> 4;
		// let active = (parseInt(Network.mode) & 32) >> 5;
		// let free = (parseInt(Network.mode) & 64) >> 6;
		// let excent = (parseInt(Network.mode) & 128) >> 7;

		// console.warn('Network.nowMode: ', Network.nowMode);
		let main = (parseInt(Network.nowMode) & 1);
		let pause = (parseInt(Network.nowMode) & 2) >> 1;
		let pause2 = (parseInt(Network.nowMode) & 8) >> 3;
		let home = (parseInt(Network.nowMode) & 16) >> 4;
		// let main = (parseInt(Network.nowMode) & 32) >> 5;
		let down = (parseInt(Network.nowMode) & 64) >> 6;
		let upper = (parseInt(Network.nowMode) & 128) >> 7;
		let hot = (parseInt(Network.nowMode) & 256) >> 8;

		let train = (parseInt(Network.nowMode) & 512) >> 9;
		let comfort = (parseInt(Network.nowMode) & 1024) >> 10;
		let pause3 = (parseInt(Network.nowMode) & 2048) >> 11;

		if (pause) {
			rows.push(this.renderIcon(require('./../../assets/ic-mode-pause.png'), {}));
		}
		if (pause2) {
			rows.push(this.renderIcon(require('./../../assets/ic-mode-pause.png'), {marginLeft: rows.length ? Common.getLengthByIPhone7(10) : 0}));
		}
		if (pause3) {
			str = str + 'pause2\r\n';
			rows.push(this.renderIcon(require('./../../assets/ic-mode-pause.png'), {marginLeft: rows.length ? Common.getLengthByIPhone7(10) : 0}));
		}
		if (home) {
			rows.push(this.renderIcon(require('./../../assets/ic-mode-to-home.png'), {marginLeft: rows.length ? Common.getLengthByIPhone7(10) : 0}));
		}
		if (main && !pause) {
			rows.push(this.renderIcon(require('./../../assets/ic-mode-curl.png'), {marginLeft: rows.length ? Common.getLengthByIPhone7(10) : 0}));
		}
		if (down) {
			rows.push(this.renderIcon(require('./../../assets/ic-mode-length-down.png'), {marginLeft: rows.length ? Common.getLengthByIPhone7(10) : 0}));
		}
		if (upper) {
			rows.push(this.renderIcon(require('./../../assets/ic-mode-length-up.png'), {marginLeft: rows.length ? Common.getLengthByIPhone7(10) : 0}));
		}
		if (hot) {
			rows.push(this.renderIcon(require('./../../assets/ic-mode-hot.png'), {marginLeft: rows.length ? Common.getLengthByIPhone7(10) : 0}));
		}
		if (train) {
			rows.push(this.renderIcon(require('./../../assets/ic-mode-train.png'), {marginLeft: rows.length ? Common.getLengthByIPhone7(10) : 0}));
		}
		if (comfort) {
			rows.push(this.renderIcon(require('./../../assets/ic-mode-comfort.png'), {marginLeft: rows.length ? Common.getLengthByIPhone7(10) : 0}));
		}

		this.setState({
			rowIcons: rows,
		});
		
	}

	render() {
		return (<View style={{
		flex: 1,
		width: Common.getLengthByIPhone7(0),
		// height: Dimensions.get('window').height,
		justifyContent: 'flex-start',
		alignItems: 'center',
		backgroundColor: 'white',
		}}>
			<View style={{
				width: Common.getLengthByIPhone7(0),
				height: Common.getLengthByIPhone7(90),
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'space-between',
				paddingRight: Common.getLengthByIPhone7(20),
			}}>
				<Text style={{
					marginLeft: Common.getLengthByIPhone7(20),
					color: Colors.mainColor,
					fontFamily: 'OpenSans-Bold',
					fontWeight: 'bold',
					textAlign: 'center',
					fontSize: Common.getLengthByIPhone7(57),
				}}
				allowFontScaling={false}>
					{this.toHHMM(Network.nowTime)}<Text style={{
						color: Colors.mainColor,
						fontFamily: 'OpenSans-Bold',
						fontWeight: 'bold',
						textAlign: 'center',
						fontSize: Common.getLengthByIPhone7(22),
					}}
					allowFontScaling={false}>
						{this.toSS(Network.nowTime)}
					</Text>
				</Text>
				{this.state.rowIcons}
			</View>
			<View style={{
				width: Common.getLengthByIPhone7(0),
				flex: 1,
				alignItems: 'center',
				justifyContent: 'space-between',
				backgroundColor: '#F5F9FF',
			}}>
				<Text style={{
					// color: (Network.nowLoadA > Network.limitA ? Colors.yellowColor : Colors.mainColor),
					color: Network.statusA == 1 ? Colors.mainColor : Colors.disabledColor,
					fontFamily: 'Rubik-Regular',
					fontWeight: 'bold',
					textAlign: 'center',
					fontSize: Common.getLengthByIPhone7(54),
					// marginTop: Common.getLengthByIPhone7(10),
				}}
				allowFontScaling={false}>
					{Network.nowDegA}&#0176;
				</Text>
				<View style={{
					alignItems: 'center',
					justifyContent: 'space-between',
					width: Common.getLengthByIPhone7(0),
					flexDirection: 'row',
				}}>
					<View style={{
						marginLeft: Common.getLengthByIPhone7(20),
					}}>
						<Text style={{
							color: Network.statusA == 1 ? Colors.mainColor : Colors.disabledColor,
							fontFamily: 'Rubik-Regular',
							fontWeight: 'normal',
							textAlign: 'center',
							fontSize: Common.getLengthByIPhone7(36),
						}}
						allowFontScaling={false}>
							{Network.maxDegA}&#0176;
						</Text>
						<Animated.Image
							source={require('./../../assets/ic-arrow-up.png')}
							style={{
								resizeMode: 'contain',
								width: Common.getLengthByIPhone7(60),
								height: Common.getLengthByIPhone7(90),
								tintColor: Network.statusA == 1 ? Colors.greenColor : Colors.disabledColor,
								transform: [{rotateX: this.directA.interpolate({
									inputRange: [0, 1],
									outputRange: ['180deg', '0deg'],
								})}]
								// transform: [{ rotate: (this.state.directA == 0 ? '180deg' : '0deg') }]
							}}
						/>
						<Text style={{
							color: Network.statusA == 1 ? Colors.mainColor : Colors.disabledColor,
							fontFamily: 'Rubik-Regular',
							fontWeight: 'normal',
							textAlign: 'center',
							fontSize: Common.getLengthByIPhone7(36),
							// marginLeft: Common.getLengthByIPhone7(20),
						}}
						allowFontScaling={false}>
							{Network.minDegA}&#0176;
						</Text>
					</View>
					<Image
						source={Network.currentDevice.left}
						style={{
							resizeMode: 'contain',
							width: Common.getLengthByIPhone7(145),
							height: Common.getLengthByIPhone7(145),
							marginRight: Common.getLengthByIPhone7(100),
							tintColor: Network.statusA == 1 ? null : Colors.disabledColor,
						}}
					/>
				</View>
				<View style={{
					marginBottom: Common.getLengthByIPhone7(10),
					width: Common.getLengthByIPhone7(0),
					alignItems: 'flex-end',
				}}>
					<View style={{
						alignItems: 'center',
						justifyContent: 'center',
						width: Common.getLengthByIPhone7(69),
						height: Common.getLengthByIPhone7(69),
						marginRight: Common.getLengthByIPhone7(20),
					}}>
						<Image
							source={require('./../../assets/ic-weight.png')}
							style={{
								resizeMode: 'contain',
								width: Common.getLengthByIPhone7(69),
								height: Common.getLengthByIPhone7(69),
								position: 'absolute',
								tintColor: Network.statusA == 1 ? (Network.nowLoadA > Network.limitA ? Colors.yellowColor : Colors.mainColor) : Colors.disabledColor,
							}}
						/>
						<Text style={{
							color: 'white',
							fontFamily: 'OpenSans-Bold',
							fontWeight: 'normal',
							textAlign: 'center',
							fontSize: Common.getLengthByIPhone7(14),
							marginTop: Common.getLengthByIPhone7(15),
							// marginRight: Common.getLengthByIPhone7(5),
						}}
						allowFontScaling={false}>
							{Network.nowLoadA}%
						</Text>
					</View>
				</View>
			</View>
			<View style={{
				width: Common.getLengthByIPhone7(0),
				height: Common.getLengthByIPhone7(100),
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'space-between',
				// backgroundColor: 'red',
			}}>
				<TouchableOpacity style={{
					width: Common.getLengthByIPhone7(60),
					height: Common.getLengthByIPhone7(60),
					borderRadius: Common.getLengthByIPhone7(10),
					alignItems: 'center',
					justifyContent: 'center',
					// borderWidth: 2,
					elevation: 3,
					backgroundColor: 'white',
					marginLeft: Common.getLengthByIPhone7(20),
				}}
				onPress={() => {
					this.props.navigation.navigate('Help');
				}}>
					<Text style={{
						color: Colors.mainColor,
						fontFamily: 'OpenSans-Bold',
						fontWeight: 'bold',
						textAlign: 'center',
						fontSize: Common.getLengthByIPhone7(24),
					}}
					allowFontScaling={false}>
						?
					</Text>
				</TouchableOpacity>
				<StartStopButton
					started={Network.inWork == 1 && Network.inPause == 1 ? 0 : (Network.inWork == 0 ? 0 : 1)}
					onStart={() => {
						// Network.sendCommand('raise_ev 5\r\n');
						Network.addComand('raise_ev 5\r\n');
					}}
					onStop={() => {
						// Network.sendCommand('raise_ev 8\r\n');
						Network.addComand('raise_ev 8\r\n');
					}}
					onPause={() => {
						// Network.sendCommand('raise_ev 7\r\n');
						Network.addComand('raise_ev 7\r\n');
					}}
				/>
				<TouchableOpacity style={{
					width: Common.getLengthByIPhone7(60),
					height: Common.getLengthByIPhone7(60),
					borderRadius: Common.getLengthByIPhone7(10),
					alignItems: 'center',
					justifyContent: 'center',
					// borderWidth: 2,
					elevation: 3,
					backgroundColor: 'white',
					marginRight: Common.getLengthByIPhone7(20),
				}}
				onPress={() => {
					this.props.navigation.navigate('DeviceFastSettings');
				}}>
					<Image
						source={require('./../../assets/ic-ab.png')}
						style={{
							resizeMode: 'contain',
							width: Common.getLengthByIPhone7(48),
							height: Common.getLengthByIPhone7(48),
						}}
					/>
				</TouchableOpacity>
			</View>
		</View>);
	}
}
