import React from 'react';
import {
  Platform,
  Text,
  View,
  BackHandler,
  Alert,
  RefreshControl,
  FlatList,
  Image,
  NativeModules,
  NativeEventEmitter,
  PermissionsAndroid,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';

export default class StartStopButton extends React.Component {

  state = {
    status: 0,
  };

  UNSAFE_componentWillMount() {
    this.setState({
        status: this.props.started,
    });
  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
        status: props.started,
    });
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);

  }

  render() {
    return (<View style={{
        // width: Common.getLengthByIPhone7(170),
        // height: Common.getLengthByIPhone7(80),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }}>
        <TouchableOpacity style={{
            width: Common.getLengthByIPhone7(70),
            height: Common.getLengthByIPhone7(70),
            borderRadius: Common.getLengthByIPhone7(10),
            marginRight: Common.getLengthByIPhone7(19),
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'red',
            elevation: 6,
        }}
        onPress={() => {
            if (this.props.onStop) {
                this.props.onStop();
            }
        }}>
            <View style={{
                width: Common.getLengthByIPhone7(15),
                height: Common.getLengthByIPhone7(15),
                backgroundColor: 'white',
            }} />
        </TouchableOpacity>
        {this.state.status === 0 ? (<TouchableOpacity style={{
            width: Common.getLengthByIPhone7(70),
            height: Common.getLengthByIPhone7(70),
            borderRadius: Common.getLengthByIPhone7(10),
            marginRight: Common.getLengthByIPhone7(19),
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#1EBA17',
            elevation: 6,
        }}
        onPress={() => {
            if (this.props.onStart) {
                this.props.onStart();
            }
        }}>
            <Image
                source={require('./../assets/ic-start.png')}
                style={{
                    resizeMode: 'contain',
                    width: Common.getLengthByIPhone7(17),
                    height: Common.getLengthByIPhone7(27),
                }}
            />
        </TouchableOpacity>) : (<TouchableOpacity style={{
            width: Common.getLengthByIPhone7(70),
            height: Common.getLengthByIPhone7(70),
            borderRadius: Common.getLengthByIPhone7(10),
            marginRight: Common.getLengthByIPhone7(19),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#FFC929',
            elevation: 6,
        }}
        onPress={() => {
            if (this.props.onPause) {
                this.props.onPause();
            }
        }}>
            <View style={{
                width: Common.getLengthByIPhone7(6),
                height: Common.getLengthByIPhone7(27),
                marginRight: Common.getLengthByIPhone7(4),
                backgroundColor: 'white',
            }} />
            <View style={{
                width: Common.getLengthByIPhone7(6),
                height: Common.getLengthByIPhone7(27),
                backgroundColor: 'white',
            }} />
        </TouchableOpacity>)}
    </View>);
  }
}
