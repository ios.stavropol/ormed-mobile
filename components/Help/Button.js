import React from 'react';
import {
  Platform,
  Text,
  View,
  BackHandler,
  Alert,
  RefreshControl,
  Linking,
  Image,
  NativeModules,
  NativeEventEmitter,
  PermissionsAndroid,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';

export default class Button extends React.Component {

  state = {
    active: false,
  };

  UNSAFE_componentWillMount() {

  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);

  }

  render() {
		return (<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(319),
			height: Common.getLengthByIPhone7(89),
			borderRadius: Common.getLengthByIPhone7(10),
			backgroundColor: (this.state.active ? Colors.mainColor : 'white'),
			shadowColor: '#D7DEEC',
			shadowOffset: { width: 0, height: Common.getLengthByIPhone7(5) },
			shadowOpacity: 1,
			shadowRadius: Common.getLengthByIPhone7(10),
			elevation: 4,
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'flex-start'
		}, this.props.style]}
		activeOpacity={1}
		onPressIn={() => {
			this.setState({
				active: true,
			});
		}}
		onPressOut={() => {
			this.setState({
				active: false,
			});
		}}
		onPress={() => {
			if (this.props.action) {
				this.props.action();
			}
		}}>
			<Image
			source={(this.state.active ? this.props.activeIcon : this.props.inactiveIcon)}
			style={{
				resizeMode: 'contain',
				width: Common.getLengthByIPhone7(51),
				height: Common.getLengthByIPhone7(51),
				marginLeft: Common.getLengthByIPhone7(17),
			}}
			/>
			<Text style={{
				color: (this.state.active ? 'white' : Colors.mainColor),
				fontFamily: 'OpenSans-Bold',
				fontWeight: 'bold',
				textAlign: 'left',
				fontSize: Common.getLengthByIPhone7(18),
				marginLeft: Common.getLengthByIPhone7(35),
			}}
			allowFontScaling={false}>
				{this.props.title}
			</Text>
		</TouchableOpacity>
	);
  }
}
