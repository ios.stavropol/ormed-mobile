import React from 'react';
import {
  Text,
  View,
  Image,
  Alert,
  Animated,
  Easing,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network, { getCalendar } from './../../Utilites/Network';
import { observer } from 'mobx-react';
import Modal from 'react-native-modal';
import MultiSlider from '@ptomasroos/react-native-multi-slider';

@observer
export default class PauseChangeModalView extends React.Component {
    
  state = {
    refresh: false,
    show: false,
    pause: 0,
    values: 0,
  };

  UNSAFE_componentWillMount() {
    this.setState({
        show: this.props.show,
        pause: this.props.pause,
        values: [Network.pause],
    });
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  render() {
    
    return (<View style={{
        // flex: 1,
      }}>
        <Modal isVisible={this.state.show}>
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <TouchableOpacity style={{
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    backgroundColor: 'transparent',
                }}
                onPress={() => {
                    this.setState({
                        text: '',
                        companies: [],
                    }, () => {
                        
                    });
                    if (this.props.onClose) {
                        this.props.onClose();
                    }
                }}
                />
                <View style={{
                    width: Common.getLengthByIPhone7(212),// - Common.getLengthByIPhone7(40),
                    padding: Common.getLengthByIPhone7(20),
                    borderRadius: Common.getLengthByIPhone7(10),
                    backgroundColor: 'white',
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                }}>
                    <Text style={{
                      color: Colors.mainColor,
                      fontFamily: 'Rubik-Regular',
                      fontWeight: 'bold',
                      textAlign: 'center',
                      fontSize: Common.getLengthByIPhone7(12),
                      marginBottom: Common.getLengthByIPhone7(40),
                    }}
                    allowFontScaling={false}>
                      Изменение длительности паузы (сек)
                    </Text>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'flex-end',
                    }}>
                        <MultiSlider
                            sliderLength={Common.getLengthByIPhone7(160)}
                            min={0}
                            max={60}
                            selectedStyle={{
                                backgroundColor: 'rgba(77, 148, 254, 0.5)',
                                height: Common.getLengthByIPhone7(30),
                                borderRadius: Common.getLengthByIPhone7(10),
                                borderColor: Colors.mainColor,
                                borderWidth: 1,
                            }}
                            unselectedStyle={{
                                backgroundColor: 'white',
                                height: Common.getLengthByIPhone7(30),
                                borderColor: Colors.mainColor,
                                borderWidth: 1,
                                borderRadius: Common.getLengthByIPhone7(10),
                            }}
                            customLabel={label => {
                                return (<View style={{
                                    height: Common.getLengthByIPhone7(31),
                                    width: Common.getLengthByIPhone7(40),
                                    borderRadius: Common.getLengthByIPhone7(5),
                                    backgroundColor: '#CDE1FF',
                                    position: 'absolute',
                                    left: label.oneMarkerLeftPosition - Common.getLengthByIPhone7(20),
                                    bottom: Common.getLengthByIPhone7(45),
                                    // zIndex: 200,
                                }}>
                                    <Text style={{
                                        color: Colors.mainColor,
                                        fontFamily: 'OpenSans-Bold',
                                        fontWeight: 'bold',
                                        textAlign: 'center',
                                        fontSize: Common.getLengthByIPhone7(10),
                                        lineHeight: Common.getLengthByIPhone7(22),
                                        marginTop: Common.getLengthByIPhone7(3),
                                    }}
                                    allowFontScaling={false}>
                                        {label.oneMarkerValue}
                                    </Text>
                                </View>);
                            }}
                            customMarkerLeft={() => {
                                return (<View style={{
                                    height: Common.getLengthByIPhone7(32),
                                    width: Common.getLengthByIPhone7(32),
                                    borderRadius: Common.getLengthByIPhone7(16),
                                    marginTop: Common.getLengthByIPhone7(30),
                                    backgroundColor: 'white',
                                    borderColor: Colors.mainColor,
                                    borderWidth: 2,
                                    zIndex: 100,
                                }} />);
                            }}
                            values={this.state.values}
                            isMarkersSeparated={true}
                            enableLabel={true}
                            vertical={false}
                            bottom={0}
                            onValuesChange={values => {
                                console.warn('onValuesChange: ', values);
                                this.setState({
                                    values,
                                });
                            }}
                            onValuesChangeStart={values => {
                            
                            }}
                            onValuesChangeFinish={values => {
                                
                            }}
                        />
                        <Text style={{
                            color: 'white',
                            fontFamily: 'OpenSans-Regular',
                            fontWeight: 'normal',
                            textAlign: 'center',
                            fontSize: Common.getLengthByIPhone7(13),
                            position: 'absolute',
                            left: Common.getLengthByIPhone7(10),
                            bottom: Common.getLengthByIPhone7(18),
                            zIndex: 0,
                        }}
                        allowFontScaling={false}>
                            0
                        </Text>
                        <Text style={{
                            color: Colors.mainColor,
                            fontFamily: 'OpenSans-Regular',
                            fontWeight: 'normal',
                            textAlign: 'center',
                            fontSize: Common.getLengthByIPhone7(13),
                            position: 'absolute',
                            right: Common.getLengthByIPhone7(10),
                            bottom: Common.getLengthByIPhone7(18),
                            zIndex: 0,
                        }}
                        allowFontScaling={false}>
                            60
                        </Text>
                    </View>
                    <View style={{
                        marginTop: Common.getLengthByIPhone7(30),
                        width: Common.getLengthByIPhone7(180),
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                    }}>
                        <TouchableOpacity style={{
                            width: Common.getLengthByIPhone7(82),
                            height: Common.getLengthByIPhone7(38),
                            borderRadius: Common.getLengthByIPhone7(10),
                            backgroundColor: 'white',
                            elevation: 3,
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                        onPress={() => {
                            if (this.props.onClose) {
                                this.props.onClose();
                            }
                        }}>
                            <Text style={{
                                color: Colors.mainColor,
                                fontFamily: 'Rubik-Regular',
                                fontWeight: 'bold',
                                textAlign: 'center',
                                fontSize: Common.getLengthByIPhone7(10),
                            }}
                            allowFontScaling={false}>
                                Отмена
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{
                            width: Common.getLengthByIPhone7(82),
                            height: Common.getLengthByIPhone7(38),
                            borderRadius: Common.getLengthByIPhone7(10),
                            backgroundColor: 'white',
                            elevation: 3,
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                        onPress={() => {
                            if (this.props.onSave) {
                                this.props.onSave(this.state.values[0]);
                            }
                        }}>
                            <Text style={{
                                color: Colors.mainColor,
                                fontFamily: 'Rubik-Regular',
                                fontWeight: 'bold',
                                textAlign: 'center',
                                fontSize: Common.getLengthByIPhone7(10),
                            }}
                            allowFontScaling={false}>
                                Сохранить
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>
      </View>);
  }
}
