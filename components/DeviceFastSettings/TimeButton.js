import React from 'react';
import {
  Text,
  View,
  Image,
  Alert,
  Animated,
  Easing,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';
import DatePicker from 'react-native-date-picker';

export default class TimeButton extends React.Component {
    
  state = {
    open: false,
    date: new Date(),
    time: 0,
  };

  UNSAFE_componentWillMount() {
    let date = new Date();
    let secs = Network.time;
    var sec_num = parseInt(secs, 10);
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor(sec_num / 60) % 60;
    date.setHours(hours);
    date.setMinutes(minutes);
    this.setState({
      date,//: new Date(),
      time: this.props.time,
    });
  }

  UNSAFE_componentWillUnmount() {

  }

  UNSAFE_componentWillReceiveProps(props) {
    let date = new Date();
    let secs = Network.time;
    var sec_num = parseInt(secs, 10);
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor(sec_num / 60) % 60;
    date.setHours(hours);
    date.setMinutes(minutes);
    this.setState({
      open: props.open,
      date,
      time: props.time,
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.open !== nextState.open || this.state.time !== nextProps.time) { 
      return true;
    }
    return false;
  }

  constructor(props) {
    super(props);
  }

  toHHMM = secs => {
    if (secs == 0) {
        return '00:00';
    }
    var sec_num = parseInt(secs, 10)
    var hours   = Math.floor(sec_num / 3600)
    var minutes = Math.floor(sec_num / 60) % 60
    sec_num = '00';//sec_num < 10 ? '0' + sec_num : sec_num;
    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    return hours + ':' + minutes + ':' + sec_num;
}

toSS = secs => {
    if (secs == 0) {
        return '00';
    }
    var sec_num = parseInt(secs, 10)
    var seconds = sec_num % 60;
    seconds = seconds < 10 ? '0' + seconds : seconds;
    return seconds;
}

  render() {
    
    return (<TouchableOpacity style={[{
        width: Common.getLengthByIPhone7(104),
        height: Common.getLengthByIPhone7(48),
        borderRadius: Common.getLengthByIPhone7(10),
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 3,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
      }, this.props.style]}
      onPress={() => {
        // this.props.onClick();
        this.setState({
          open: true,
        }, () => {
          // if (this.TimePicker) {
          //   this.TimePicker.open();
          // }
        });
      }}>
        <Image
            source={require('./../../assets/ic-mode-velocity.png')}
            style={{
                resizeMode: 'contain',
                // tintColor: this.props.style.tintColor,
                width: Common.getLengthByIPhone7(17),
                height: Common.getLengthByIPhone7(17),
            }}
        />
        <Text style={{
            color: Colors.mainColor,
            fontFamily: 'OpenSans-Bold',
            fontWeight: 'bold',
            textAlign: 'center',
            fontSize: Common.getLengthByIPhone7(16),
            marginLeft: Common.getLengthByIPhone7(7),
        }}
        allowFontScaling={false}>
          {this.toHHMM(this.state.time)}
        </Text>
      <DatePicker
        modal
        mode={'time'}
        textColor={Colors.mainColor}
        is24hourSource={true}
        open={this.state.open}
        date={this.state.date}
        title={'Изменение времени'}
        confirmText={'Готово'}
        cancelText={'Отмена'}
        onConfirm={date => {
          this.setState({
            open: false,
            date,
          }, () => {
            if (this.props.onChange) {
              this.props.onChange(date);
            }
          });
        }}
        onCancel={() => {
          this.setState({
            open: false,
          });
        }}
      />
    </TouchableOpacity>);
  }
}