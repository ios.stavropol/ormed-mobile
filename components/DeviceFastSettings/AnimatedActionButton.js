import React from 'react';
import {
  Text,
  View,
  Image,
  Alert,
  Animated,
  Easing,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';

export default class AnimatedActionButton extends React.Component {
    
  state = {
    active: false,
  };

  UNSAFE_componentWillMount() {
    this.setState({
        // show: this.props.show,
        // motor: this.props.motor,
        // values: this.props.motor == 0 ? [Network.limitA] : [Network.limitB],
    });
  }

  UNSAFE_componentWillUnmount() {

  }

  UNSAFE_componentWillReceiveProps(props) {
    
  }

  constructor(props) {
    super(props);

  }

  render() {
    
    return (<TouchableOpacity style={[{
        width: Common.getLengthByIPhone7(48),// - Common.getLengthByIPhone7(50))/4,
        height: Common.getLengthByIPhone7(48),
        borderRadius: Common.getLengthByIPhone7(10),
        backgroundColor: (this.state.active ? Colors.mainColor : 'white'),
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 3,
      }, this.props.style]}
      activeOpacity={1}
		onPressIn={() => {
			this.setState({
				active: true,
			});
		}}
	  onPressOut={() => {
		this.setState({
			active: false,
		}, () => {
			setTimeout(() => {
				if (this.props.onPress) {
					this.props.onPress();
				}
			}, 100);
		});
      }}>
        <Image
            source={this.props.image}
            style={{
                resizeMode: 'contain',
                tintColor: (this.state.active ? 'white' : Colors.mainColor),
                width: Common.getLengthByIPhone7(30),
                height: Common.getLengthByIPhone7(30),
            }}
        />
        <Text style={{
            color: this.props.style.tintColor,
            fontFamily: 'OpenSans-Bold',
            fontWeight: 'bold',
            textAlign: 'center',
            fontSize: Common.getLengthByIPhone7(7),
            position: 'absolute',
            bottom: Common.getLengthByIPhone7(2),
        }}
        allowFontScaling={false}>
          {this.props.title}
        </Text>
      </TouchableOpacity>);
  }
}