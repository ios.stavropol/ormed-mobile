import React from 'react';
import {
  Platform,
  Text,
  View,
  Animated,
  Alert,
  RefreshControl,
  FlatList,
  Image,
  NativeModules,
  NativeEventEmitter,
  PermissionsAndroid,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';
import {PanGestureHandler, State} from 'react-native-gesture-handler';

const markerSize = Common.getLengthByIPhone7(20);
const balloonWidth = Common.getLengthByIPhone7(40);

export default class Slider extends React.Component {

	action = State.UNDETERMINED;
	left = new Animated.Value(0);
	left2 = new Animated.Value(0);

  state = {
    status: 0,
	x: 0,
	width: 0,
	min: 0,
	max: 0,
	value: 0,
	valueRaw: 0,
  };

  UNSAFE_componentWillMount() {
    this.setState({
        min: this.props.min,
		max: this.props.max,
		value: this.props.value,
		valueRaw: this.props.valueRaw,
    }, () => {
		setTimeout(() => {
			let left = (this.state.value / (this.state.max - this.state.min)) * this.state.width - balloonWidth / 2;
			let left2 = (this.state.value / (this.state.max - this.state.min)) * this.state.width;
			if (left2 < 0) {
				left2 = 0;
			}
			Animated.parallel([
				Animated.timing(this.left, {
					toValue: left,
					duration: 0,
					useNativeDriver: false,
				}),
				Animated.timing(this.left2, {
					toValue: left2,
					duration: 0,
					useNativeDriver: false,
				})
			]).start();
		}, 100);
	});
  }

  UNSAFE_componentWillReceiveProps(props) {
	  if (this.state.valueRaw != props.value) {
		this.setState({
			min: props.min,
			max: props.max,
			value: props.value,
			valueRaw: props.valueRaw,
		}, () => {
			setTimeout(() => {
				let left = (this.state.value / (this.state.max - this.state.min)) * this.state.width - balloonWidth / 2;
				let left2 = (this.state.value / (this.state.max - this.state.min)) * this.state.width;
				if (left2 < 0) {
					left2 = 0;
				}
				Animated.parallel([
					Animated.timing(this.left, {
						toValue: left,
						duration: 0,
						useNativeDriver: false,
					}),
					Animated.timing(this.left2, {
						toValue: left2,
						duration: 0,
						useNativeDriver: false,
					})
				]).start();
			}, 100);
		});
	  }
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);

  }

  render() {
    return (
			<View style={{
				flexDirection: 'row',
				alignItems: 'center',
				// backgroundColor: 'red'
			}}
			onLayout={event => {
				let {x, y, width, height} = event.nativeEvent.layout;
				this.setState({
					x,
					width,
				});
			}}>
				<View style={[{
					width: Common.getLengthByIPhone7(216),
					height: Common.getLengthByIPhone7(13),
					borderRadius: Common.getLengthByIPhone7(10),
					alignItems: 'center',
					justifyContent: 'flex-start',
					borderWidth: 1,
					borderColor: Colors.mainColor,
					backgroundColor: '#F5F9FF',
					overflow: 'hidden',
				}, this.props.style]}>
					<Animated.View style={{
						backgroundColor: '#4D94FE',
						opacity: 0.5,
						width: this.left2,
						height: Common.getLengthByIPhone7(13),
						position: 'absolute',
						left: 0,
					}} />
				</View>
				<PanGestureHandler
					maxPointers={1}
					onHandlerStateChange={({nativeEvent}) => {
						this.action = nativeEvent.state;
						if (nativeEvent.state === State.BEGAN) {
							// this.action = State.BEGAN;
						} else if (nativeEvent.state === State.ACTIVE) {
							// this.action = State.ACTIVE;
						} else if (nativeEvent.state === State.END) {
							let left = nativeEvent.absoluteX - this.state.x + balloonWidth / 2;
							let value = (this.state.max - this.state.min) * (left / this.state.width);
							// console.warn(left);
							if (value < this.state.min) {
								value = this.state.min;
							} else if (value > this.state.max) {
								value = this.state.max;
							}
							this.setState({
								value: Math.ceil(value),
							});
							if (this.props.onChange) {
								this.props.onChange(value);
							}
						}
					}}
					onGestureEvent={({nativeEvent}) => {
						if (nativeEvent.state === State.ACTIVE) {
							let left = nativeEvent.absoluteX - this.state.x;
							let left2 = nativeEvent.absoluteX - this.state.x + balloonWidth / 2;
							if (left2 < 0) {
								left2 = 0;
							}
							if ((left >= -balloonWidth / 2) && (left <= this.state.width - balloonWidth / 2)) {
								Animated.parallel([
									Animated.timing(this.left, {
										toValue: left,
										duration: 0,
										useNativeDriver: false,
									}),
									Animated.timing(this.left2, {
										toValue: left2,
										duration: 0,
										useNativeDriver: false,
									})
								]).start();
								let value = (this.state.max - this.state.min) * (left2 / this.state.width);
								// console.warn(left);
								if (value < this.state.min) {
									value = this.state.min;
								} else if (value > this.state.max) {
									value = this.state.max;
								}
								this.setState({
									value: Math.ceil(value),
								});
							}
						}
					}}
				>
					<Animated.View style={{
						position: 'absolute',
						left: this.left,
						bottom: -(markerSize - Common.getLengthByIPhone7(13)) / 2,
						alignItems: 'center',
					}}>
						<View style={{
							backgroundColor: '#CDE1FF',
							width: balloonWidth,
							height: Common.getLengthByIPhone7(30),
							borderRadius: Common.getLengthByIPhone7(5),
							marginBottom: -(markerSize - Common.getLengthByIPhone7(13)) / 2,
							alignItems: 'center',
						}}>
							<Text style={{
								color: Colors.mainColor,
								fontFamily: 'OpenSans-Bold',
								fontWeight: 'bold',
								textAlign: 'center',
								fontSize: Common.getLengthByIPhone7(15),
								lineHeight: Common.getLengthByIPhone7(22),
								marginTop: Common.getLengthByIPhone7(3),
							}}
							allowFontScaling={false}>
								{this.state.value}%
							</Text>
						</View>
						<View style={{
							width: markerSize,
							height: markerSize,
							borderRadius: markerSize / 2,
							backgroundColor: 'white',
							elevation: 9,
							borderColor: Colors.mainColor,
							borderWidth: 2,
						}} />
					</Animated.View>
				</PanGestureHandler>
			</View>
    );
  }
}
