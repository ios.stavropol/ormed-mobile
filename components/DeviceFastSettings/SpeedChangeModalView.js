import React from 'react';
import {
  Text,
  View,
  Image,
  Alert,
  Animated,
  Easing,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network, { getCalendar } from './../../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
import Modal from 'react-native-modal';
import MultiSlider from '@ptomasroos/react-native-multi-slider';

@observer
export default class SpeedChangeModalView extends React.Component {
    
  state = {
    refresh: false,
    show: false,
    velocity: 0,
    values: [0],
  };

  UNSAFE_componentWillMount() {
    this.setState({
        show: this.props.show,
        velocity: this.props.velocity,
        values: [Network.velocity],
    });
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  render() {
    
    return (<View style={{
        // flex: 1,
      }}>
        <Modal isVisible={this.state.show}>
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <TouchableOpacity style={{
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    backgroundColor: 'transparent',
                }}
                onPress={() => {
                    this.setState({
                        text: '',
                        companies: [],
                    }, () => {
                        
                    });
                    if (this.props.onClose) {
                        this.props.onClose();
                    }
                }}
                />
                <View style={{
                    width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                    padding: Common.getLengthByIPhone7(20),
                    borderRadius: Common.getLengthByIPhone7(10),
                    backgroundColor: 'white',
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                }}>
                    <Text style={{
                      color: Colors.mainColor,
                      fontFamily: 'Rubik-Regular',
                      fontWeight: 'bold',
                      textAlign: 'center',
                      fontSize: Common.getLengthByIPhone7(18),
                      marginBottom: Common.getLengthByIPhone7(40),
                    }}
                    allowFontScaling={false}>
                      Изменение скорости мотора
                    </Text>
                    <MultiSlider
                        sliderLength={Common.getLengthByIPhone7(250)}
                        min={0}
                        max={100}
                        selectedStyle={{
                            backgroundColor: Colors.mainColor,
                            height: 10,
                            borderRadius: 5,
                        }}
                        unselectedStyle={{
                            backgroundColor: 'white',
                            height: 10,
                            borderColor: Colors.mainColor,
                            borderWidth: 1,
                        }}
                        markerContainer={{
                            height: 20,
                            width: 20,
                            backgroundColor: Colors.mainColor,
                        }}
                        marker={{
                            height: 20,
                            width: 20,
                            backgroundColor: Colors.mainColor,
                        }}
                        customMarkerLeft={() => {
                            return (<View style={{
                                height: 20,
                                width: 20,
                                borderRadius: 10,
                                marginTop: 10,
                                backgroundColor: Colors.mainColor,
                            }} />);
                        }}
                        values={this.state.values}
                        isMarkersSeparated={true}
                        enableLabel={true}
                        vertical={false}
                        bottom={0}
                        onValuesChange={values => {
                            console.warn('onValuesChange: ', values);
                            this.setState({
                                values,
                            });
                        }}
                        onValuesChangeStart={values => {
                        
                        }}
                        onValuesChangeFinish={values => {
                            
                        }}
                    />
                    <View style={{
                        width: Common.getLengthByIPhone7(300),
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                    }}>
                        <TouchableOpacity style={{
                            width: Common.getLengthByIPhone7(130),
                            height: Common.getLengthByIPhone7(40),
                            borderRadius: Common.getLengthByIPhone7(10),
                            borderColor: Colors.mainColor,
                            borderWidth: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                        onPress={() => {
                            if (this.props.onClose) {
                                this.props.onClose();
                            }
                        }}>
                            <Text style={{
                                color: Colors.mainColor,
                                fontFamily: 'Rubik-Regular',
                                fontWeight: 'bold',
                                textAlign: 'center',
                                fontSize: Common.getLengthByIPhone7(18),
                            }}
                            allowFontScaling={false}>
                                Отмена
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{
                            width: Common.getLengthByIPhone7(130),
                            height: Common.getLengthByIPhone7(40),
                            borderRadius: Common.getLengthByIPhone7(10),
                            borderColor: Colors.mainColor,
                            borderWidth: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                        onPress={() => {
                            if (this.props.onSave) {
                                this.props.onSave(this.state.values);
                            }
                        }}>
                            <Text style={{
                                color: Colors.mainColor,
                                fontFamily: 'Rubik-Regular',
                                fontWeight: 'bold',
                                textAlign: 'center',
                                fontSize: Common.getLengthByIPhone7(18),
                            }}
                            allowFontScaling={false}>
                                Сохранить
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>
      </View>);
  }
}
