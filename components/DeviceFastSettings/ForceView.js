import React from 'react';
import {
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';

export default class ForceView extends React.Component {

  state = {
    limit: 100,
	disabled: false,
  };

  UNSAFE_componentWillMount() {
    this.setState({
      limit: this.props.value,
	  disabled: this.props.disabled,
    });
  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
		limit: props.value,
		disabled: props.disabled,
	});
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);
  }

  render() {
	  let str = '';

	  if (this.state.limit == 100) {
		str = String.fromCharCode(0x221e);
	  } else {
		str = this.state.limit + '%';
	  }
    return (
		<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(48),
			height: Common.getLengthByIPhone7(48),
			borderRadius: Common.getLengthByIPhone7(10),
			alignItems: 'center',
			justifyContent: 'center',
			backgroundColor: 'white',
			elevation: 3,
		}, this.props.style]}
		activeOpacity={this.state.disabled ? 1 : 0.8}
		onPress={() => {
			if (!this.state.disabled) {
				if (this.props.onClick) {
					this.props.onClick();
				}
			}
		}}>
		<Image
			source={require('./../../assets/ic-weight.png')}
			style={{
				resizeMode: 'contain',
				width: Common.getLengthByIPhone7(36),
				height: Common.getLengthByIPhone7(36),
				tintColor: this.state.disabled ? Colors.disabledColor : Colors.mainColor,
			}}
		/>
		<Text style={{
			color: 'white',
			fontFamily: 'Rubik-Regular',
			fontWeight: 'bold',
			textAlign: 'center',
			fontSize: Common.getLengthByIPhone7(8),
			position: 'absolute',
			// right: Common.getLengthByIPhone7(20),
			bottom: Common.getLengthByIPhone7(14),
		}}
		allowFontScaling={false}>
			{str}
		</Text>
		</TouchableOpacity>
	);
  }
}
