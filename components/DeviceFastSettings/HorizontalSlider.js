import React from 'react';
import {
  Text,
  View,
  Animated,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import {PanGestureHandler, State} from 'react-native-gesture-handler';

const markerSize = Common.getLengthByIPhone7(35);
const balloonWidth = Common.getLengthByIPhone7(34);

export default class HorizontalSlider extends React.Component {

	action = State.UNDETERMINED;
	leftLeft = new Animated.Value(0);
	left2Left = new Animated.Value(0);

	translateX = new Animated.Value(0)

  state = {
    status: 0,
	x: 0,
	width: 0,
	min: 0,
	max: 0,
	valueLeft: 0,
	valueLeftRaw: 0,
  };

  UNSAFE_componentWillMount() {
    this.setState({
        min: this.props.min,
		max: this.props.max,
		valueLeft: this.props.value,
		valueLeftRaw: this.props.value,
    }, () => {
		this.calculateValues();
	});
  }

  UNSAFE_componentWillReceiveProps(props) {
		this.setState({
			min: props.min,
			max: props.max,
			valueLeft: this.state.valueLeftRaw != props.value ? props.value : this.state.valueLeft,
			valueLeftRaw: this.state.valueLeftRaw != props.value ? props.value : this.state.valueLeftRaw,
		}, () => {
			this.calculateValues();
		});
		this.container.measure((ox, oy, width, height, px, py) => {
			// console.warn('px: ', px);
			this.setState({
				x: px,
				width,
			});
		});
  }

	UNSAFE_componentDidMount() {
		setTimeout(() => {
			this.container.measure((ox, oy, width, height, px, py) => {
				// console.warn('px: ', px);
				this.setState({
					x: px,
					width,
				});
			});
		}, 1000);
	}

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);

  }

  calculateValues = () => {
	setTimeout(() => {
		let leftLeft = ((this.state.valueLeft - this.state.min) / (this.state.max - this.state.min)) * this.state.width - balloonWidth / 2;
		let left2Left = ((this.state.valueLeft - this.state.min) / (this.state.max - this.state.min)) * this.state.width;
		if (left2Left < 0) {
			left2Left = 0;
		}
		// console.warn('leftLeft: ', leftLeft);
		Animated.parallel([
			Animated.timing(this.leftLeft, {
				toValue: leftLeft,
				duration: 0,
				useNativeDriver: false,
			}),
			Animated.timing(this.left2Left, {
				toValue: left2Left,
				duration: 0,
				useNativeDriver: false,
			}),
		]).start();
	}, 100);
  }

  renderLabel = (position, value) => {
	return (<View style={{
		backgroundColor: '#CDE1FF',
		height: Common.getLengthByIPhone7(28),
		width: balloonWidth,
		borderRadius: Common.getLengthByIPhone7(5),
		// marginBottom: -(markerSize - Common.getLengthByIPhone7(13)) / 2,
		alignItems: 'center',
	}}>
		<Text style={{
			color: Colors.mainColor,
			fontFamily: 'OpenSans-Bold',
			fontWeight: 'bold',
			textAlign: 'center',
			fontSize: Common.getLengthByIPhone7(10),
			lineHeight: Common.getLengthByIPhone7(22),
			marginTop: Common.getLengthByIPhone7(3),
		}}
		allowFontScaling={false}>
			{value}{this.props.measure}
		</Text>
	</View>);
  }

  render() {

	let leftLabelMin = null;

	leftLabelMin = (this.renderLabel(this.props.position, this.state.valueLeft));

    return (
		<View style={{
			zIndex: 1000,
		}}
		ref={el => this.container = el}
		onLayout={params => {
			
		}}>
			<View style={{
				flexDirection: 'row',
				alignItems: 'center',
				// backgroundColor: 'red'
			}}>
				<View style={[{
					width: Common.getLengthByIPhone7(186),
					height: Common.getLengthByIPhone7(33),
					borderRadius: Common.getLengthByIPhone7(10),
					flexDirection: 'row',
					alignItems: 'center',
					justifyContent: 'space-between',
					borderWidth: 1,
					borderColor: Colors.mainColor,
					backgroundColor: '#F5F9FF',
					overflow: 'hidden',
				}, this.props.style]}>
					<Animated.View style={{
						backgroundColor: '#4D94FE',
						opacity: 0.5,
						bottom: 0,
						height: Common.getLengthByIPhone7(33),
						position: 'absolute',
						left: 0,
						width: this.left2Left,
					}} />
					<Text style={{
						color: 'white',
						fontFamily: 'OpenSans-Regular',
						fontWeight: 'normal',
						textAlign: 'center',
						fontSize: Common.getLengthByIPhone7(13),
						// lineHeight: Common.getLengthByIPhone7(22),
						marginLeft: Common.getLengthByIPhone7(10),
					}}
					allowFontScaling={false}>
						{this.state.min}{this.props.measure}
					</Text>
					<Text style={{
						color: Colors.mainColor,
						fontFamily: 'OpenSans-Regular',
						fontWeight: 'normal',
						textAlign: 'center',
						fontSize: Common.getLengthByIPhone7(13),
						// lineHeight: Common.getLengthByIPhone7(22),
						marginRight: Common.getLengthByIPhone7(10),
					}}
					allowFontScaling={false}>
						{this.state.max}{this.props.measure}
					</Text>
				</View>
				<PanGestureHandler
					maxPointers={1}
					activeOffsetX={[0, 0]}
					onHandlerStateChange={({nativeEvent}) => {
						console.warn('onHandlerStateChange: ', nativeEvent.state);
						this.action = nativeEvent.state;
						if (nativeEvent.state === State.BEGAN) {
							if (this.props.onStart) {
								this.props.onStart();
							}
						} else if (nativeEvent.state === State.ACTIVE) {
							// this.action = State.ACTIVE;
						} else if (nativeEvent.state === State.END) {
							if (this.props.onChange) {
								this.props.onChange(this.state.valueLeft);
							}
						}
					}}
					onGestureEvent={({nativeEvent}) => {
						if (nativeEvent.state === State.ACTIVE) {
							// console.warn(JSON.stringify(nativeEvent));
							// Animated.event([
							// 	{
							// 		nativeEvent: {
							// 			// translationX: this.translateX,
							// 		}
							// 	}], 
							// 	{ useNativeDriver: true }
							// );
							// let leftLeft = this.state.width - (nativeEvent.absoluteX - this.state.x);
							let leftLeft = (nativeEvent.absoluteX - this.state.x);
							let left2Left = (nativeEvent.absoluteX - this.state.x + balloonWidth / 2);
							if (left2Left < 0) {
								left2Left = 0;
							}
							// console.warn(leftLeft);
							if ((leftLeft >= -balloonWidth / 2) && (leftLeft <= this.state.width - balloonWidth / 2)) {
								// if ((leftLeft + balloonWidth / 2 + markerSize / 2) >= (parseFloat(JSON.stringify(this.leftRight)) + balloonWidth / 2 - markerSize / 2)) {
								// 	return;
								// }
								Animated.parallel([
									Animated.timing(this.leftLeft, {
										toValue: leftLeft,
										duration: 0,
										useNativeDriver: false,
									}),
									Animated.timing(this.left2Left, {
										toValue: left2Left,
										duration: 0,
										useNativeDriver: false,
									}),
								]).start();
								let valueLeft = this.state.min + (this.state.max - this.state.min) * (left2Left / this.state.width);
								// console.warn(left);
								if (valueLeft < this.state.min) {
									valueLeft = this.state.min;
								} else if (valueLeft > this.state.max) {
									valueLeft = this.state.max;
								}
								this.setState({
									valueLeft: Math.floor(valueLeft),
								});
							}
						}
					}}
				>
					<Animated.View style={{
						position: 'absolute',
						left: this.leftLeft,
						bottom:  -(markerSize - Common.getLengthByIPhone7(33)) / 2,
						alignItems: 'center',
						justifyContent: 'center',
						// flexDirection: 'row',
						// transform:[
						// 	{
						// 		translateX : this.translateX
						// 	}
						// ]
					}}>
						{leftLabelMin}
						<View style={{
							width: markerSize,
							height: markerSize,
							borderRadius: markerSize / 2,
							backgroundColor: 'white',
							elevation: 9,
							borderColor: Colors.mainColor,
							borderWidth: 2,
						}} />
					</Animated.View>
				</PanGestureHandler>
			</View>
		</View>
    );
  }
}