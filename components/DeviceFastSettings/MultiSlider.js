import React from 'react';
import {
  Text,
  View,
  Animated,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import {PanGestureHandler, State} from 'react-native-gesture-handler';

const markerSize = Common.getLengthByIPhone7(36);
const balloonHeight = Common.getLengthByIPhone7(30);

export default class MultiSlider extends React.Component {

	action = State.UNDETERMINED;
	leftLeft = new Animated.Value(0);
	left2Left = new Animated.Value(0);
	leftRight = new Animated.Value(0);
	left2Right = new Animated.Value(0);

	translateX = new Animated.Value(0)
	translateY = new Animated.Value(0)

  state = {
    status: 0,
	y: 0,
	height: 0,
	min: 0,
	max: 0,
	valueLeft: 0,
	valueRight: 0,
	valueLeftRaw: 0,
	valueRightRaw: 0,
	disabled: false,
  };

  UNSAFE_componentWillMount() {
    this.setState({
        min: this.props.min,
		max: this.props.max,
		valueLeft: this.props.valueMin,
		valueRight: this.props.valueMax,
		valueLeftRaw: this.props.valueMin,
		valueRightRaw: this.props.valueMax,
		disabled: this.props.disabled,
    }, () => {
		this.calculateValues();
	});
  }

  UNSAFE_componentWillReceiveProps(props) {
		this.setState({
			min: props.min,
			max: props.max,
			valueLeft: this.state.valueLeftRaw != props.valueMin ? props.valueMin : this.state.valueLeft,
			valueLeftRaw: this.state.valueLeftRaw != props.valueMin ? props.valueMin : this.state.valueLeftRaw,
			valueRight: this.state.valueRightRaw != props.valueMax ? props.valueMax : this.state.valueRight,
			valueRightRaw: this.state.valueRightRaw != props.valueMax ? props.valueMax : this.state.valueRightRaw,
			disabled: props.disabled,
		}, () => {
			this.calculateValues();
		});
		this.container.measure((ox, oy, width, height, px, py) => {
			// console.warn(px);
			this.setState({
				y: py,
				height,
			});
		});
  }

  UNSAFE_componentDidMount() {
	// this.container.measure(data => {
	// 	console.warn(data);
	// });
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);

  }

  calculateValues = () => {
	setTimeout(() => {
		let leftLeft = ((this.state.valueLeft - this.state.min) / (this.state.max - this.state.min)) * this.state.height - balloonHeight / 2;
		let left2Left = ((this.state.valueLeft - this.state.min) / (this.state.max - this.state.min)) * this.state.height;
		if (left2Left < 0) {
			left2Left = 0;
		}
		let leftRight = ((this.state.valueRight - this.state.min) / (this.state.max - this.state.min)) * this.state.height - balloonHeight / 2;
		let left2Right = ((this.state.valueRight - this.state.min) / (this.state.max - this.state.min)) * this.state.height;
		if (left2Right > this.state.height) {
			left2Right = this.state.height;
		}
		Animated.parallel([
			Animated.timing(this.leftLeft, {
				toValue: leftLeft,
				duration: 0,
				useNativeDriver: false,
			}),
			Animated.timing(this.left2Left, {
				toValue: left2Left,
				duration: 0,
				useNativeDriver: false,
			}),
			Animated.timing(this.leftRight, {
				toValue: leftRight,
				duration: 0,
				useNativeDriver: false,
			}),
			Animated.timing(this.left2Right, {
				toValue: this.state.height - left2Right,
				duration: 0,
				useNativeDriver: false,
			})
		]).start();
	}, 100);
  }

  renderLabel = (position, value) => {
	return (<View style={{
		backgroundColor: this.state.disabled ? '#F2F3F3' : '#CDE1FF',
		width: Common.getLengthByIPhone7(40),
		height: balloonHeight,
		borderRadius: Common.getLengthByIPhone7(5),
		// marginBottom: -(markerSize - Common.getLengthByIPhone7(13)) / 2,
		alignItems: 'center',
	}}>
		<Text style={{
			color: this.state.disabled ? Colors.disabledColor : Colors.mainColor,
			fontFamily: 'OpenSans-Bold',
			fontWeight: 'bold',
			textAlign: 'center',
			fontSize: Common.getLengthByIPhone7(15),
			lineHeight: Common.getLengthByIPhone7(22),
			marginTop: Common.getLengthByIPhone7(3),
		}}
		allowFontScaling={false}>
			{value}&#0176;
		</Text>
	</View>);
  }

  render() {

	let leftLabelMin = null;
	let leftLabelMax = null;
	let rightLabelMin = null;
	let rightLabelMax = null;

	if (this.props.position === 'left') {
		leftLabelMin = (this.renderLabel(this.props.position, this.state.valueLeft));
		leftLabelMax = (this.renderLabel(this.props.position, this.state.valueRight));
	} else {
		rightLabelMin = (this.renderLabel(this.props.position, this.state.valueLeft));
		rightLabelMax = (this.renderLabel(this.props.position, this.state.valueRight));
	}

    return (
		<View style={[{
			zIndex: 1000,
		}, this.props.style]}
		ref={el => this.container = el}
		onLayout={params => {
			
		}}>
			<View style={{
				flexDirection: 'row',
				alignItems: 'center',
				// backgroundColor: 'red'
			}}>
				<View style={{
					height: this.props.style.height,// Common.getLengthByIPhone7(235),
					width: Common.getLengthByIPhone7(33),
					borderRadius: Common.getLengthByIPhone7(10),
					alignItems: 'center',
					justifyContent: 'space-between',
					borderWidth: 1,
					borderColor: this.state.disabled ? Colors.disabledColor : Colors.mainColor,
					backgroundColor: this.state.disabled ? 'white' : '#F5F9FF',
					overflow: 'hidden',
				}}>
					<Text style={{
						color: this.state.disabled ? Colors.disabledColor : Colors.mainColor,
						fontFamily: 'OpenSans-Regular',
						fontWeight: 'normal',
						textAlign: 'center',
						fontSize: Common.getLengthByIPhone7(12),
						marginTop: Common.getLengthByIPhone7(5),
					}}
					allowFontScaling={false}>
						{this.props.max}&#0176;
					</Text>
					<Animated.View style={{
						backgroundColor: this.state.disabled ? '#EEEFF0' : '#4D94FE',
						opacity: 0.5,
						top: this.left2Right,
						bottom: this.left2Left,
						width: Common.getLengthByIPhone7(33),
						position: 'absolute',
						left: 0,
					}} />
					<Text style={{
						color: this.state.disabled ? Colors.disabledColor : Colors.mainColor,
						fontFamily: 'OpenSans-Regular',
						fontWeight: 'normal',
						textAlign: 'center',
						fontSize: Common.getLengthByIPhone7(12),
						marginBottom: Common.getLengthByIPhone7(5),
					}}
					allowFontScaling={false}>
						{this.props.min}&#0176;
					</Text>
				</View>
				<PanGestureHandler
					maxPointers={1}
					enabled={!this.state.disabled}
					onHandlerStateChange={({nativeEvent}) => {
						this.action = nativeEvent.state;
						if (nativeEvent.state === State.BEGAN) {
							if (this.props.onStart) {
								this.props.onStart();
							}
						} else if (nativeEvent.state === State.ACTIVE) {
							// this.action = State.ACTIVE;
						} else if (nativeEvent.state === State.END) {
							if (this.props.onChange) {
								this.props.onChange([this.state.valueLeft, this.state.valueRight]);
							}
						}
					}}
					onGestureEvent={({nativeEvent}) => {
						if (nativeEvent.state === State.ACTIVE) {
							// console.warn(JSON.stringify(nativeEvent));
							let leftRight = this.state.height - (nativeEvent.absoluteY - this.state.y);
							let left2Right = this.state.height - (nativeEvent.absoluteY - this.state.y - balloonHeight / 2);
							if (left2Right > this.state.height) {
								left2Right = this.state.height;
							}
							// console.warn(leftRight);
							if ((leftRight >= -balloonHeight / 2) && (leftRight <= this.state.height - balloonHeight / 2)) {
								if ((leftRight - balloonHeight / 2 + markerSize / 2) <= (parseFloat(JSON.stringify(this.leftLeft)) + balloonHeight / 2 + markerSize / 2)) {
									return;
								}
								Animated.parallel([
									Animated.timing(this.leftRight, {
										toValue: leftRight,
										duration: 0,
										useNativeDriver: false,
									}),
									Animated.timing(this.left2Right, {
										toValue: this.state.height - left2Right,
										duration: 0,
										useNativeDriver: false,
									})
								]).start();
								let valueRight = this.state.min + (this.state.max - this.state.min) * (left2Right / this.state.height);
								// console.warn(left);
								if (valueRight < this.state.min) {
									valueRight = this.state.min;
								} else if (valueRight > this.state.max) {
									valueRight = this.state.max;
								}
								this.setState({
									valueRight: Math.ceil(valueRight),
								});
							}
						}
					}}
				>
					<Animated.View style={{
						position: 'absolute',
						bottom: this.leftRight,
						left: (this.props.position === 'left' ? -(markerSize - Common.getLengthByIPhone7(33)) / 2 : null),
						right: (this.props.position === 'right' ? -(markerSize - Common.getLengthByIPhone7(33)) / 2 : null),
						alignItems: 'center',
						justifyContent: 'center',
						flexDirection: 'row',
					}}>
						{rightLabelMax}
						<View style={{
							width: markerSize,
							height: markerSize,
							borderRadius: markerSize / 2,
							backgroundColor: 'white',
							elevation: 9,
							borderColor: this.state.disabled ? Colors.disabledColor : Colors.mainColor,
							borderWidth: 2,
						}} />
						{leftLabelMax}
					</Animated.View>
				</PanGestureHandler>
				<PanGestureHandler
					maxPointers={1}
					enabled={!this.state.disabled}
					onHandlerStateChange={({nativeEvent}) => {
						this.action = nativeEvent.state;
						if (nativeEvent.state === State.BEGAN) {
							if (this.props.onStart) {
								this.props.onStart();
							}
						} else if (nativeEvent.state === State.ACTIVE) {
							// this.action = State.ACTIVE;
						} else if (nativeEvent.state === State.END) {
							if (this.props.onChange) {
								this.props.onChange([this.state.valueLeft, this.state.valueRight]);
							}
						}
					}}
					onGestureEvent={({nativeEvent}) => {
						if (nativeEvent.state === State.ACTIVE) {
							// console.warn(JSON.stringify(nativeEvent));
							// Animated.event([
							// 	{
							// 		nativeEvent: {
							// 			// translationX: this.translateX,
							// 			translationY: this.translateY
							// 		}
							// 	}], 
							// 	{ useNativeDriver: true }
							// );
							let leftLeft = this.state.height - (nativeEvent.absoluteY - this.state.y);
							let left2Left = this.state.height - (nativeEvent.absoluteY - this.state.y - balloonHeight / 2);
							if (left2Left < 0) {
								left2Left = 0;
							}
							// console.warn(leftLeft);
							if ((leftLeft >= -balloonHeight / 2) && (leftLeft <= this.state.height - balloonHeight / 2)) {
								if ((leftLeft + balloonHeight / 2 + markerSize / 2) >= (parseFloat(JSON.stringify(this.leftRight)) + balloonHeight / 2 - markerSize / 2)) {
									return;
								}
								Animated.parallel([
									Animated.timing(this.leftLeft, {
										toValue: leftLeft,
										duration: 0,
										useNativeDriver: false,
									}),
									Animated.timing(this.left2Left, {
										toValue: left2Left,
										duration: 0,
										useNativeDriver: false,
									}),
								]).start();
								let valueLeft = this.state.min + (this.state.max - this.state.min) * (left2Left / this.state.height);
								// console.warn(left);
								if (valueLeft < this.state.min) {
									valueLeft = this.state.min;
								} else if (valueLeft > this.state.max) {
									valueLeft = this.state.max;
								}
								this.setState({
									valueLeft: Math.floor(valueLeft),
								});
							}
						}
					}}
				>
					<Animated.View style={{
						position: 'absolute',
						bottom: this.leftLeft,
						left: (this.props.position === 'left' ? -(markerSize - Common.getLengthByIPhone7(33)) / 2 : null),
						right: (this.props.position === 'right' ? -(markerSize - Common.getLengthByIPhone7(33)) / 2 : null),
						alignItems: 'center',
						justifyContent: 'center',
						flexDirection: 'row',
						transform:[
							{
								translateY : this.translateY
							},
							// {
							// 	translateX : this.translateX
							// }
						]
					}}>
						{rightLabelMin}
						<View style={{
							width: markerSize,
							height: markerSize,
							borderRadius: markerSize / 2,
							backgroundColor: 'white',
							elevation: 9,
							borderColor: this.state.disabled ? Colors.disabledColor : Colors.mainColor,
							borderWidth: 2,
						}} />
						{leftLabelMin}
					</Animated.View>
				</PanGestureHandler>
			</View>
		</View>
    );
  }
}