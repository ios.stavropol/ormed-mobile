import React from 'react';
import {
  Text,
  View,
  Image,
  Alert,
  Animated,
  Easing,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';

const width = Common.getLengthByIPhone7(220);

export default class VelocityView extends React.Component {
    
    timer = null;

  state = {
    refresh: false,
    velocity: 0,
    velocity2: 0,
  };

  UNSAFE_componentWillMount() {
    this.setState({
        velocity: this.props.velocity,
        velocity2: this.props.velocity,
    });
  }

  UNSAFE_componentWillUnmount() {
    
  }

  UNSAFE_componentWillReceiveProps(props) {
      if (this.state.velocity2 != props.velocity) {
        this.setState({
            velocity: props.velocity,
            velocity2: props.velocity,
        });
      }
  }

  constructor(props) {
    super(props);

  }

   getInitialState(){
     return{
       pressAction: new Animated.Value(0),
       value: 0,
     };
   }

   addOne = () => {
       if (this.state.velocity < 100) {
            this.setState({
                velocity: this.state.velocity + 1
            });
            this.timer = setTimeout(this.addOne, 200);
       }
    }

    minusOne = () => {
        if (this.state.velocity > 0) {
             this.setState({
                 velocity: this.state.velocity - 1
             });
             this.timer = setTimeout(this.minusOne, 200);
        }
     }

  render() {
    
    return (<View style={{
        width: width,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    }}>
        <Text style={{
            color: Colors.mainColor,
            fontFamily: 'OpenSans-Bold',
            fontWeight: 'bold',
            textAlign: 'center',
            fontSize: Common.getLengthByIPhone7(12),
            marginBottom: Common.getLengthByIPhone7(12),
        }}
        allowFontScaling={false}>
            Скорость моторов
        </Text>
        <View style={{
            width: width,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            // backgroundColor: '#CDE1FF',
            // borderRadius: Common.getLengthByIPhone7(18),
            overflow: 'hidden'
        }}>
            <View style={{
                height: Common.getLengthByIPhone7(22),
                width: width,
                backgroundColor: '#CDE1FF',
                position: 'absolute',
                left: 0,
                borderRadius: Common.getLengthByIPhone7(11),
                overflow: 'hidden'
            }}>
                <View style={{
                    height: Common.getLengthByIPhone7(22),
                    width: width*this.state.velocity/100,
                    backgroundColor: '#A1C7FF',
                    position: 'absolute',
                    left: 0,
                    // borderTopLeftRadius: Common.getLengthByIPhone7(11),
                    // borderBottomLeftRadius: Common.getLengthByIPhone7(11),
                }} />
            </View>
            <TouchableOpacity style={{
                width: Common.getLengthByIPhone7(25),
                height: Common.getLengthByIPhone7(25),
                borderRadius: Common.getLengthByIPhone7(25)/2,
                backgroundColor: Colors.mainColor,
                alignItems: 'center',
                justifyContent: 'center',
            }}
            onPressIn={() => {
                this.minusOne();
            }}
            onPressOut={() => {
                clearTimeout(this.timer);
                if (this.props.onChange) {
                    this.props.onChange(this.state.velocity);
                }
            }}>
                <Image
                    source={require('./../assets/ic-minus.png')}
                    style={{
                        resizeMode: 'contain',
                        width: Common.getLengthByIPhone7(12),
                        height: Common.getLengthByIPhone7(12),
                    }}
                />
            </TouchableOpacity>
            <Text style={{
                color: 'white',
                fontFamily: 'OpenSans-Bold',
                fontWeight: 'bold',
                textAlign: 'center',
                fontSize: Common.getLengthByIPhone7(14),
            }}
            allowFontScaling={false}>
                {this.state.velocity}%
            </Text>
            <TouchableOpacity style={{
                width: Common.getLengthByIPhone7(25),
                height: Common.getLengthByIPhone7(25),
                borderRadius: Common.getLengthByIPhone7(25)/2,
                backgroundColor: Colors.mainColor,
                alignItems: 'center',
                justifyContent: 'center',
            }}
            onPressIn={() => {
                this.addOne();
            }}
            onPressOut={() => {
                clearTimeout(this.timer);
                if (this.props.onChange) {
                    this.props.onChange(this.state.velocity);
                }
            }}>
                <Image
                    source={require('./../assets/ic-plus.png')}
                    style={{
                        resizeMode: 'contain',
                        width: Common.getLengthByIPhone7(15),
                        height: Common.getLengthByIPhone7(15),
                    }}
                />
            </TouchableOpacity>
        </View>
    </View>);
  }
}