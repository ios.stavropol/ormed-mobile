import React from 'react';
import {
  Image,
  TouchableOpacity,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';
import { observer } from 'mobx-react';

@observer
export default class StatusMotor extends React.Component {

  state = {
    status: 0,
  };

  UNSAFE_componentWillMount() {
    this.setState({
        status: this.props.status,
    });
  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
        status: props.status,
    });
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);
  }

  render() {
    return (
        <TouchableOpacity style={{
          width: Common.getLengthByIPhone7(48),
          height: Common.getLengthByIPhone7(48),
          borderRadius: Common.getLengthByIPhone7(10),
          marginTop: Common.getLengthByIPhone7(10),
          alignItems: 'center',
          justifyContent: 'center',
          elevation: 3,
          backgroundColor: 'white',
        }}
        onPress={() => {
            if (this.props.onClick) {
                this.props.onClick();
            }
        }}>
        <Image
            source={this.state.status == 1 ? require('./../assets/ic-check2.png') : require('./../assets/ic-close2.png')}
            style={{
              // tintColor: this.state.status == 1 ? null : Colors.mainColor,
              resizeMode: 'contain',
              width: Common.getLengthByIPhone7(35),
              height: Common.getLengthByIPhone7(35),
            }}
        />
        </TouchableOpacity>
    );
  }
}
