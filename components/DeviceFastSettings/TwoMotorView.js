import React from 'react';
import {
  Platform,
  Text,
  View,
  BackHandler,
  Alert,
  RefreshControl,
  Linking,
  Image,
  NativeModules,
  NativeEventEmitter,
  TouchableOpacity,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
// import { bleManager } from 'react-native-ble-plx';
import BleManager from 'react-native-ble-manager';
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
import { stringToBytes, bytesToString } from 'convert-string';
import StartStopButton from '../Device/StartStopButton';
import Toast from 'react-native-simple-toast';
import StatusMotor from './StatusMotor';
import LoadChangeModalView from './LoadChangeModalView';
import SpeedChangeModalView from './SpeedChangeModalView';
import PauseChangeModalView from './PauseChangeModalView';
import ActionButton from './ActionButton';
import AnimatedActionButton from './AnimatedActionButton';
import TimeButton from './TimeButton';
import MultiSlider from './MultiSlider';
import HorizontalSlider from './HorizontalSlider';
import ForceView from './ForceView';
import {GestureHandlerRootView, ScrollView} from 'react-native-gesture-handler';

@observer
export default class TwoMotorView extends React.Component {

  timer = null;

  state = {
    loading: false,
    reload: false,
    statusA: 0,
    statusB: 0,
    showLoad: false,
    showSpeed: false,
    showPause: false,
    scrollEnabled: true,
    open: false,
    date: new Date(),
    maxADeg: 175,
    minADeg: 30,
    maxBDeg: 90,
    minBDeg: -90,
  };

  UNSAFE_componentWillMount() {
    this.setState({
      statusA: Network.statusA,
      statusB: Network.statusB,
	  reload: this.props.reload,
	  maxADeg: this.props.maxADeg,
	  minADeg: this.props.minADeg,
	  maxBDeg: this.props.maxBDeg,
	  minBDeg: this.props.minBDeg,
    });
  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      reload: props.reload,
	  maxADeg: props.maxADeg,
	  minADeg: props.minADeg,
	  maxBDeg: props.maxBDeg,
	  minBDeg: props.minBDeg,
	  statusA: Network.statusA,
      statusB: Network.statusB,
    });
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);
  }

  setMinMax = (param, value) => {
    if (param == 30) {
      this.setState({
        maxADeg: parseInt(value),
      });
      // Network.sendCommand('get_devpar 31\r\n');//maxB
      Network.addComand('get_devpar 31\r\n');
    } else if (param == 31) {
      this.setState({
        maxBDeg: parseInt(value),
      });
      // Network.sendCommand('get_devpar 25\r\n');//minA
      Network.addComand('get_devpar 25\r\n');
    } else if (param == 25) {
      this.setState({
        minADeg: parseInt(value),
      });
      // Network.sendCommand('get_devpar 26\r\n');//minB
      Network.addComand('get_devpar 26\r\n');
    } else if (param == 26) {
      this.setState({
        minBDeg: parseInt(value),
      });
    }
  }

  render() {
    return (<View style={{
      flex: 1,
      width: Common.getLengthByIPhone7(0),
      // height: Dimensions.get('window').height,
      justifyContent: 'flex-start',
      alignItems: 'center',
      backgroundColor: 'white',
    }}>
        <ScrollView style={{
            width: Common.getLengthByIPhone7(0),
            flex: 1,
        }}
        scrollEnabled={this.state.scrollEnabled}
        contentContainerStyle={{

        }}>
            <View style={{
                width: Common.getLengthByIPhone7(0),
                height: Common.getLengthByIPhone7(300),
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                borderBottomWidth: 1,
                borderBottomColor: 'rgba(0, 0, 0, 0.3)'
            }}>
              <View style={{
                width: Common.getLengthByIPhone7(0)/2,
                height: Common.getLengthByIPhone7(300),
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                borderRightWidth: 1,
                borderRightColor: 'rgba(0, 0, 0, 0.3)',
                position: 'absolute',
                left: 0,
                top: 0,
                // backgroundColor: 'red',
              }}>
                <View style={{
                  // height: Common.getLengthByIPhone7(290),
                  // marginLeft: -Common.getLengthByIPhone7(10),
                  // width: Common.getLengthByIPhone7(0)/2,
                  // backgroundColor: 'red',
                  // position: 'absolute',
                  // left: 0,//Common.getLengthByIPhone7(10),
                  // backgroundColor: 'red',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                  <MultiSlider
                    style={{
                      height: Common.getLengthByIPhone7(235),
                      marginLeft: Common.getLengthByIPhone7(17),
                      // backgroundColor: 'red',
                    }}
                    position={'left'}
                    min={this.state.minADeg}
                    max={this.state.maxADeg}
                    valueMin={Network.minDegA}
                    valueMax={Network.maxDegA}
                    disabled={Network.statusA == 0}
                    onStart={() => {
                      console.warn('onStart');
                      this.setState({
                        scrollEnabled: false,
                      });
                    }}
                    onChange={values => {
                      if (values[0] != Network.minDegA) {
                        // Toast.showWithGravity('v0: '+values[0]+' minA: '+Network.minDegA, 1, Toast.TOP);
                        // Network.sendCommand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + values[0] + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                        Network.addComand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + values[0] + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                      } else if (values[1] != Network.maxDegA) {
                        // Toast.showWithGravity('v1: '+values[1]+' maxA: '+Network.maxDegA, 1, Toast.TOP);
                        // Network.sendCommand('set_prcsettings ' + values[1] + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                        Network.addComand('set_prcsettings ' + values[1] + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                      }
                      Toast.showWithGravity('Параметр изменен!', 1, Toast.TOP);
                      this.setState({
                        scrollEnabled: true,
                      });
                    }}
                  />
                </View>
                <View style={{
                  // width: Common.getLengthByIPhone7(0)/2,
                  height: Common.getLengthByIPhone7(300),
                  // flexDirection: 'row',
                  // backgroundColor: 'red',
                  alignItems: 'flex-end',
                  justifyContent: 'space-between',
                  // position: 'absolute',
                  marginRight: Common.getLengthByIPhone7(15),
                  // top: 0,
                }}>
                  <StatusMotor style={{

                    }}
                    status={Network.statusA}
                    onClick={() => {
                      // Network.sendCommand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + (Network.statusA == 1 ? 0 : 1) + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                      Network.addComand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + (Network.statusA == 1 ? 0 : 1) + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                      Network.statusA = Network.statusA == 1 ? 0 : 1;
                      this.setState({
                        reload: !this.state.reload,
                      });
                    }}
                  />
                  <Image
                    source={Network.currentDevice.left}
                    style={{
                        resizeMode: 'contain',
                        width: Common.getLengthByIPhone7(93),
                        height: Common.getLengthByIPhone7(98),
                        tintColor: Network.statusA == 1 ? null : Colors.disabledColor,
                    }}
                  />
                  <ForceView
                    style={{
                      marginBottom: Common.getLengthByIPhone7(14),
                    }}
                    value={Network.limitA}
                    disabled={Network.statusA == 0}
                    onClick={() => {
                      this.setState({
                        showLoad: true,
                        motor: 0,
                      });
                    }}
                  />
                </View>
              </View>
              <View style={{
                width: Common.getLengthByIPhone7(0)/2,
                height: Common.getLengthByIPhone7(300),
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                position: 'absolute',
                right: 0,
                top: 0,
                // backgroundColor: 'red',
              }}>
                <View style={{
                  height: Common.getLengthByIPhone7(300),
                  alignItems: 'flex-start',
                  justifyContent: 'space-between',
                  // position: 'absolute',
                  marginLeft: Common.getLengthByIPhone7(15),
                  // top: 0,
                }}>
                  <StatusMotor style={{

                    }}
                    status={Network.statusB}
                    onClick={() => {
                      // Network.sendCommand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + (Network.statusB == 1 ? 0 : 1) + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                      Network.addComand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + (Network.statusB == 1 ? 0 : 1) + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                      Network.statusB = Network.statusB == 1 ? 0 : 1;
                      this.setState({
                        reload: !this.state.reload,
                      });
                    }}
                  />
                  <Image
                    source={Network.currentDevice.right}
                    style={{
                        resizeMode: 'contain',
                        width: Common.getLengthByIPhone7(93),
                        height: Common.getLengthByIPhone7(98),
                        tintColor: Network.statusB == 1 ? null : Colors.disabledColor,
                    }}
                  />
                  <ForceView
                    style={{
                      marginBottom: Common.getLengthByIPhone7(14),
                    }}
                    value={Network.limitB}
                    disabled={Network.statusB == 0}
                    onClick={() => {
                      this.setState({
                        showLoad: true,
                        motor: 1,
                      });
                    }}
                  />
                </View>
                <View style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                  <MultiSlider
                    style={{
                      height: Common.getLengthByIPhone7(235),
                      marginRight: Common.getLengthByIPhone7(17),
                    }}
                    position={'right'}
                    min={this.state.minBDeg}
                    max={this.state.maxBDeg}
                    valueMin={Network.minDegB}
                    valueMax={Network.maxDegB}
                    disabled={Network.statusB == 0}
                    onStart={() => {
                      this.setState({
                        scrollEnabled: false,
                      });
                    }}
                    onChange={values => {
                      if (values[0] != Network.minDegB) {
                        // Network.sendCommand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + values[0] + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                        Network.addComand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + values[0] + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                      }
                      if (values[1] != Network.maxDegB) {
                        // Network.sendCommand('set_prcsettings ' + Network.maxDegA + ' ' + values[1] + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                        Network.addComand('set_prcsettings ' + Network.maxDegA + ' ' + values[1] + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                      }
                      Toast.showWithGravity('Параметр изменен!', 1, Toast.TOP);
                      this.setState({
                        scrollEnabled: true,
                      });
                    }}
                  />
                </View>
              </View>
            </View>
            <View style={{
              width: Common.getLengthByIPhone7(0),
              marginTop: Common.getLengthByIPhone7(14),
              paddingLeft: Common.getLengthByIPhone7(20),
              paddingRight: Common.getLengthByIPhone7(20),
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
              <View style={{
                marginTop: Common.getLengthByIPhone7(30),
                alignItems: 'flex-start'
              }}>
                <HorizontalSlider
                  style={{

                  }}
                  measure={'%'}
                  min={0}
                  max={100}
                  value={Network.velocity}
                  onChange={velocity => {
                    // Network.sendCommand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                    Network.addComand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                    Toast.showWithGravity('Параметр изменен!', 1, Toast.TOP);
                  }}
                />
                <Text style={{
                  color: Colors.mainColor,
                  fontFamily: 'OpenSans-Bold',
                  fontWeight: 'bold',
                  textAlign: 'center',
                  fontSize: Common.getLengthByIPhone7(12),
                  // lineHeight: Common.getLengthByIPhone7(22),
                  marginTop: Common.getLengthByIPhone7(10),
                }}
                allowFontScaling={false}>
                  Скорость моторов
                </Text>
              </View>
              <TimeButton
                style={{
                  // marginTop: Common.getLengthByIPhone7(22),
                }}
                time={Network.time}
                onChange={date => {
                  let hh = date.getHours();
                  let mm = date.getMinutes();
                  let ss = date.getSeconds();
                  console.warn('hh: ', hh, ' mm: ', mm, ' ss: ', ss);
                  let secs = hh*60*60 + mm*60 + ss;
                  // Network.sendCommand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + secs + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                  Network.addComand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + secs + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                  Toast.showWithGravity('Параметр изменен!', 1, Toast.TOP);
                }}
              />
            </View>
            <View style={{
              marginTop: Common.getLengthByIPhone7(16),
              width: Common.getLengthByIPhone7(0),
              padding: Common.getLengthByIPhone7(10),
              paddingLeft: Common.getLengthByIPhone7(40),
              paddingRight: Common.getLengthByIPhone7(40),
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
              <ActionButton
                style={{
                  backgroundColor: Network.pause == 0 ? 'white' : Colors.mainColor,
                  tintColor: Network.pause == 0 ? Colors.mainColor : 'white',
                }}
                title={Network.pause + ' сек'}
                onPress={() => {
                  this.setState({
                    showPause: true,
                  });
                }}
                image={require('./../assets/ic-mode-pause.png')}
              />
              <ActionButton
                style={{
                  backgroundColor: ((Network.mode & 4) >> 2) == 1 ? Colors.mainColor : 'white',
                  tintColor: ((Network.mode & 4) >> 2) == 1 ? 'white' : Colors.mainColor,
                }}
                onPress={() => {
                  let mode = parseInt(Network.mode);
                  mode ^= (1 << 2);
                  // Network.sendCommand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                  Network.addComand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                }}
                image={require('./../assets/ic-mode-hot.png')}
              />
              <ActionButton
                style={{
                  backgroundColor: ((Network.mode & 16) >> 4) == 1 ? Colors.mainColor : 'white',
                  tintColor: ((Network.mode & 16) >> 4) == 1 ? 'white' : Colors.mainColor,
                }}
                onPress={() => {
                  let mode = parseInt(Network.mode);
                  mode ^= (1 << 4);
                  // Network.sendCommand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                  Network.addComand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                }}
                image={require('./../assets/ic-mode-train.png')}
              />
              <ActionButton
                style={{
                  backgroundColor: (Network.mode & 1) == 1 ? Colors.mainColor : 'white',
                  tintColor: (Network.mode & 1) == 1 ? 'white' : Colors.mainColor,
                }}
                onPress={() => {
                  let mode = parseInt(Network.mode);
                  mode ^= 1;
                  // Network.sendCommand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                  Network.addComand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                }}
                image={require('./../assets/ic-mode-length-up.png')}
              />
            </View>
            <View style={{
              width: Common.getLengthByIPhone7(0),
              padding: Common.getLengthByIPhone7(10),
              paddingLeft: Common.getLengthByIPhone7(40),
              paddingRight: Common.getLengthByIPhone7(40),
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
              <ActionButton
                style={{
                  backgroundColor: ((Network.mode & 2) >> 1) == 1 ? Colors.mainColor : 'white',
                  tintColor: ((Network.mode & 2) >> 1) == 1 ? 'white' : Colors.mainColor,
                }}
                onPress={() => {
                  let mode = parseInt(Network.mode);
                  mode ^= (1 << 1);
                  // Network.sendCommand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                  Network.addComand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                }}
                image={require('./../assets/ic-mode-length-down.png')}
              />
              <ActionButton
                style={{
                  backgroundColor: ((Network.mode & 8) >> 3) == 1 ? Colors.mainColor : 'white',
                  tintColor: ((Network.mode & 8) >> 3) == 1 ? 'white' : Colors.mainColor,
                }}
                onPress={() => {
                  let mode = parseInt(Network.mode);
                  mode ^= (1 << 3);
                  // Network.sendCommand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                  Network.addComand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
                }}
                image={require('./../assets/ic-mode-comfort.png')}
              />
              <AnimatedActionButton
                style={{
                  
                }}
                onPress={() => {
                  // Network.sendCommand('raise_ev 13\r\n');
                  Network.addComand('raise_ev 13\r\n');
                }}
                image={require('./../assets/ic-sett-cart.png')}
              />
              <AnimatedActionButton
                style={{
                  
                }}
                onPress={() => {
                  // Network.sendCommand('raise_ev 26\r\n');
                  Network.addComand('raise_ev 26\r\n');
                }}
                image={require('./../assets/ic-mode-home.png')}
              />
            </View>
        </ScrollView>
        <View style={{
            width: Common.getLengthByIPhone7(0),
            height: Common.getLengthByIPhone7(100),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            // backgroundColor: 'red',
            borderTopColor: 'rgba(0, 0, 0, 0.3)',
            borderTopWidth: 1,
        }}>
            <TouchableOpacity style={{
                width: Common.getLengthByIPhone7(60),
                height: Common.getLengthByIPhone7(60),
                borderRadius: Common.getLengthByIPhone7(10),
                marginLeft: Common.getLengthByIPhone7(20),
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
                // borderWidth: 2,
                elevation: 3,
            }}
            onPress={() => {
                this.props.navigation.navigate('Help');
            }}>
                <Text style={{
                    color: Colors.mainColor,
                    fontFamily: 'OpenSans-Bold',
                    fontWeight: 'bold',
                    textAlign: 'center',
                    fontSize: Common.getLengthByIPhone7(24),
                }}
                allowFontScaling={false}>
                    ?
                </Text>
            </TouchableOpacity>
            <StartStopButton
                started={Network.inWork == 1 && Network.inPause == 1 ? 0 : (Network.inWork == 0 ? 0 : 1)}
                onStart={() => {
                    // Network.sendCommand('raise_ev 5\r\n');
                    Network.addComand('raise_ev 5\r\n');
                }}
                onStop={() => {
                  // Network.sendCommand('raise_ev 8\r\n');
                  Network.addComand('raise_ev 8\r\n');
                }}
                onPause={() => {
                  // Network.sendCommand('raise_ev 7\r\n');
                  Network.addComand('raise_ev 7\r\n');
                }}
            />
            <View style={{
                width: Common.getLengthByIPhone7(60),
                height: Common.getLengthByIPhone7(60),
                marginRight: Common.getLengthByIPhone7(20),
            }} />
        </View>
        {this.state.showLoad ? (<LoadChangeModalView
          show={this.state.showLoad}
          motor={this.state.motor}
          onClose={() => {
            this.setState({
              showLoad: false,
            });
          }}
          onSave={value => {
            // Network.sendCommand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + (this.state.motor === 0 ? value : Network.limitA) + ' ' + (this.state.motor === 1 ? value : Network.limitB) + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
            Network.addComand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + (this.state.motor === 0 ? value : Network.limitA) + ' ' + (this.state.motor === 1 ? value : Network.limitB) + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
            this.setState({
              showLoad: false,
            });
          }}
        />) : null}
        {this.state.showSpeed ? (<SpeedChangeModalView
          show={this.state.showSpeed}
          velocity={Network.velocity}
          onClose={() => {
            this.setState({
              showSpeed: false,
            });
          }}
          onSave={values => {
            // Network.sendCommand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + values[0] + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
            Network.addComand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + values[0] + ' ' + Network.mode + ' ' + Network.time + ' ' + Network.pause + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
            this.setState({
              showSpeed: false,
            });
          }}
        />) : null}
        {this.state.showPause ? (<PauseChangeModalView
          show={this.state.showPause}
          velocity={Network.pause}
          onClose={() => {
            this.setState({
              showPause: false,
            });
          }}
          onSave={value => {
            // Network.sendCommand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + value + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
            Network.addComand('set_prcsettings ' + Network.maxDegA + ' ' + Network.maxDegB + ' ' + Network.minDegA + ' ' + Network.minDegB + ' ' + Network.limitA + ' ' + Network.limitB + ' ' + Network.velocity + ' ' + Network.mode + ' ' + Network.time + ' ' + value + ' ' + Network.part + ' ' + Network.pacient + ' ' + Network.statusA + ' ' + Network.statusB + ' ' + Network.nonactiveDegA + ' ' + Network.nonactiveDegB + ' ' + Network.loadNegative + ' ' + Network.loadPositive + '\r\n');
            this.setState({
              showPause: false,
            });
          }}
        />) : null}
    </View>);
  }
}
