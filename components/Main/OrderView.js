import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';

const statuses = {
    '0': {
        title: 'Новая',
        color: 'black',
    },
    '1': {
        title: 'Новая',
        color: 'green',
    },
    '2': {
        title: 'На рассмотрении',
        color: 'yellow',
    },
    '3': {
        title: 'В работе',
        color: 'red',
    },
    '4': {
        title: 'Принятие заявки',
        color: 'black',
    },
    '5': {
        title: 'Выезд на погрузку',
        color: 'black',
    },
    '6': {
      title: 'Подача транспорта',
      color: 'black',
    },
    '7': {
      title: 'Погрузка',
      color: 'black',
    },
    '8': {
      title: 'В пути',
      color: 'black',
    },
    '9': {
      title: 'Отгрузка',
      color: 'black',
    },
    '10': {
      title: 'Доставлено',
      color: 'black',
    },
    '11': {
      title: 'Отказ',
      color: 'black',
    },
};

const driverStatuses = {
  '0': {
      title: 'Новая',
      color: 'black',
  },
  '1': {
      title: 'Новая',
      color: 'black',
  },
  '2': {
      title: 'В пути',
      color: 'black',
  },
  '3': {
      title: 'Транспорт подан',
      color: 'black',
  },
  '4': {
      title: 'Погрузка',
      color: 'black',
  },
  '5': {
      title: 'Отгрузка',
      color: 'black',
  },
  '6': {
    title: 'Успешно реализована',
    color: 'black',
  },
};

export default class OrderView extends React.Component {

  state = {
    
  };

  UNSAFE_componentWillMount() {
    
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);

  }

  render() {
    // console.warn('data: '+JSON.stringify(this.props.data));

    let orderTime = new Date(this.props.data.date_supply);
    let dd = orderTime.getDate();
    dd = dd < 10 ? '0' + dd : dd;

    let mm = orderTime.getMonth() + 1;
    mm = mm < 10 ? '0' + mm : mm;

    let yyyy = orderTime.getFullYear();

    let date = dd + '.' + mm + '.' + yyyy;

    let from = '';
    let to = '';

    if (this.props.data.address_list_pickup && this.props.data.address_list_pickup.length) {
        let address = this.props.data.address_list_pickup[0];
        from = address.city_name + ', ' + address.street + ', ' + address.house;
    }

    if (this.props.data.address_list_take && this.props.data.address_list_take.length) {
        let address = this.props.data.address_list_take[0];
        to = address.city_name + ', ' + address.street + ', ' + address.house;
    }

    return (<TouchableOpacity style={{
        width: Common.getLengthByIPhone7(0),
        padding: Common.getLengthByIPhone7(20),
        // height: Common.getLengthByIPhone7(50),
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        backgroundColor: 'white',
        borderBottomColor: 'rgba(0, 0, 0, 0.2)',
        borderBottomWidth: 1,
      }}
      onPress={() => {
          if (this.props.onClick) {
            this.props.onClick(this.props.data);
          }
      }}>
        <Text style={{
          fontFamily: 'Arial',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(20),
          fontWeight: 'bold',
          color: 'black',
          marginBottom: Common.getLengthByIPhone7(10),
        }}>
          Заказ №{this.props.data.id}
        </Text>
        <Text style={{
          fontFamily: 'Arial',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(14),
          fontWeight: 'normal',
          color: 'black',
          marginBottom: Common.getLengthByIPhone7(5),
        }}>
          {date} {this.props.data.time_supply}
        </Text>
        <Text style={{
          fontFamily: 'Arial',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(14),
          fontWeight: 'normal',
          color: 'black',
          marginBottom: Common.getLengthByIPhone7(5),
        }}>
          {from} - {to}
        </Text>
        <Text style={{
          fontFamily: 'Arial',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(14),
          fontWeight: 'bold',
          color: 'black',
          marginBottom: Common.getLengthByIPhone7(5),
        }}>
            Статус заявки: <Text style={{
                fontFamily: 'Arial',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(14),
                fontWeight: 'normal',
                color: statuses[this.props.data.order_status].color,
            }}>
                {statuses[this.props.data.order_status].title}
            </Text>
        </Text>
        <Text style={{
          fontFamily: 'Arial',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(16),
          fontWeight: 'bold',
          color: 'black',
        }}>
            Стоимость: {this.props.data.price_amount}
        </Text>
    </TouchableOpacity>);
  }
}
