import React from 'react';
import {
  Text,
  View,
  Image,
  Linking,
  TouchableOpacity,
  Alert,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network, { changeStatus } from './../../Utilites/Network';
import Modal from 'react-native-modal';
import GradientButton from '../Buttons/GradientButton';
import Clipboard from '@react-native-clipboard/clipboard';
import ActionSheet from 'react-native-actionsheet';

export default class OrderModalView extends React.Component {

  state = {
    show: false,
    order: null,
    address: '',
  };

  UNSAFE_componentWillMount() {
    this.setState({
        show: this.props.show,
        order: this.props.order,
    });
  }

  UNSAFE_componentWillUnmount() {

  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      show: props.show,
      order: props.order,
    });
  }

  constructor(props) {
    super(props);

  }

  render() {

    if (this.state.order === null) {
        return null;
    }

    console.warn('order: '+JSON.stringify(this.state.order));
    let orderTime = new Date(this.state.order.date_supply);
    let dd = orderTime.getDate();
    dd = dd < 10 ? '0' + dd : dd;

    let mm = orderTime.getMonth() + 1;
    mm = mm < 10 ? '0' + mm : mm;

    let yyyy = orderTime.getFullYear();

    let date = dd + '.' + mm + '.' + yyyy;

    let from = '';
    let to = '';

    if (this.state.order.address_list_pickup && this.state.order.address_list_pickup.length) {
        let address = this.state.order.address_list_pickup[0];
        from = address.city_name + ', ' + address.street + ', ' + address.house;
    }

    if (this.state.order.address_list_take && this.state.order.address_list_take.length) {
        let address = this.state.order.address_list_take[0];
        to = address.city_name + ', ' + address.street + ', ' + address.house;
    }

    return (
    <Modal isVisible={this.state.show}>
        <View style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'flex-end',
        }}>
            <View style={{
                width: Common.getLengthByIPhone7(0),
                borderTopLeft: Common.getLengthByIPhone7(10),
                borderTopRight: Common.getLengthByIPhone7(10),
                overflow: 'hidden',
                backgroundColor: 'white',
                alignItems: 'flex-start',
                justifyContent: 'flex-end',
                marginBottom: -20,
                padding: Common.getLengthByIPhone7(16),
            }}>
                <View style={{
                    width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    // marginTop: Common.getLengthByIPhone7(40),
                }}>
                    <TouchableOpacity style={{
                        width: Common.getLengthByIPhone7(30),
                        height: Common.getLengthByIPhone7(30),
                        alignItems: 'flex-start',
                        justifyContent: 'flex-start',
                    }}
                    onPress={() => {
                        this.setState({
                            text: '',
                        }, () => {
                            
                        });
                        if (this.props.onClose) {
                            this.props.onClose();
                        }
                    }}>
                        <Image
                            source={require('./../../assets/ic-close.png')}
                            style={{
                                width: Common.getLengthByIPhone7(12),
                                height: Common.getLengthByIPhone7(12),
                                resizeMode: 'contain',
                            }}
                        />
                    </TouchableOpacity>
                </View>
                <Text style={{
                    fontFamily: 'Arial',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(20),
                    fontWeight: 'bold',
                    color: 'black',
                    marginBottom: Common.getLengthByIPhone7(10),
                }}>
                    Заказ №{this.state.order.id}
                </Text>
                <Text style={{
                    fontFamily: 'Arial',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(14),
                    fontWeight: 'normal',
                    color: 'black',
                    marginBottom: Common.getLengthByIPhone7(5),
                }}>
                {date} {this.state.order.time_supply}
                </Text>
                <Text style={{
                    fontFamily: 'Arial',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(14),
                    fontWeight: 'normal',
                    color: Colors.mainColor,
                    textDecorationLine: 'underline',
                    marginBottom: Common.getLengthByIPhone7(5),
                }}
                onPress={() => {
                    let url = 'tel:' + this.state.order.phone;
                    Linking.canOpenURL(url).then(supported => {
                        if (supported) {
                            Linking.openURL(url);
                        } else {
                            console.log("Don't know how to open URI: " + this.props.url);
                        }
                    });
                }}>
                    {this.state.order.last_name} {this.state.order.first_name} {this.state.order.patronymic}
                </Text>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'flex-start',
                    justifyContent: 'space-between',
                    width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                }}>
                    <Text style={{
                        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
                        fontFamily: 'Arial',
                        textAlign: 'left',
                        fontSize: Common.getLengthByIPhone7(14),
                        fontWeight: 'normal',
                        color: 'black',
                        marginBottom: Common.getLengthByIPhone7(5),
                    }}>
                        <Text style={{
                            fontFamily: 'Arial',
                            textAlign: 'left',
                            fontSize: Common.getLengthByIPhone7(14),
                            fontWeight: 'normal',
                            color: Colors.mainColor,
                            textDecorationLine: 'underline',
                        }}
                        onPress={() => {
                            this.setState({
                                address: from,
                            }, () => {
                                this.ActionSheet2.show();
                            });
                        }}>
                            {from}
                        </Text> - <Text style={{
                            fontFamily: 'Arial',
                            textAlign: 'left',
                            fontSize: Common.getLengthByIPhone7(14),
                            fontWeight: 'normal',
                            color: Colors.mainColor,
                            textDecorationLine: 'underline',
                        }}
                        onPress={() => {
                            this.setState({
                                address: to,
                            }, () => {
                                this.ActionSheet2.show();
                            });
                        }}>
                            {to}
                        </Text>
                    </Text>
                    <TouchableOpacity style={{

                    }}
                    onPress={() => {
                        Clipboard.setString(from + ' - ' + to);
                        Alert.alert(Config.appName, 'Адреса скопированы!');
                    }}>
                        <Image source={require('./../../assets/ic-copy.png')} style={{
                            resizeMode: 'contain',
                            width: Common.getLengthByIPhone7(20),
                            height: Common.getLengthByIPhone7(20),
                        }} />
                    </TouchableOpacity>
                </View>
                <Text style={{
                fontFamily: 'Arial',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(16),
                fontWeight: 'bold',
                color: 'black',
                }}>
                    Стоимость: {this.state.order.price_amount}
                </Text>
                <GradientButton style={{
                    width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                    height: Common.getLengthByIPhone7(60),
                    marginTop: Common.getLengthByIPhone7(20),
                    marginBottom: Common.getLengthByIPhone7(20),
                    borderRadius: Common.getLengthByIPhone7(4),
                }}
                onPress={() => {
                    this.ActionSheet.show();
                }}
                title={'Изменить статус'}/>
            </View>
        </View>
        <ActionSheet
            ref={o => this.ActionSheet = o}
            title={Config.appName}
            // options={['В пути', 'Транспорт подан', 'Погрузка', 'Отгрузка', 'Успешно реализована', 'Отмена']}
            options={['Принятие заявки', 'Выезд на погрузку', 'Подача транспорта', 'Погрузка', 'В пути', 'Отгрузка', 'Доставлено', 'Отмена']}
            cancelButtonIndex={7}
            onPress={index => {
                if(index <= 5) {
                    changeStatus(this.state.order.id, index + 4)
                    .then(() => {
                        if (this.props.onRefresh) {
                            this.props.onRefresh();
                        }
                    })
                    .catch(err => {

                    });
                } else if (index === 6) {
                    Alert.alert(
                        Config.appName,
                        "Оплата безналичная?",
                        [
                            {
                            text: "Да", onPress: () => {
                                changeStatus(this.state.order.id, index + 4, 1)
                                .then(() => {
                                    if (this.props.onRefresh) {
                                        this.props.onRefresh();
                                    }
                                })
                                .catch(err => {

                                });
                            }},
                            { text: "Нет", onPress: () => {
                                changeStatus(this.state.order.id, index + 4, 2)
                                .then(() => {
                                    if (this.props.onRefresh) {
                                        this.props.onRefresh();
                                    }
                                })
                                .catch(err => {

                                });
                            }}
                        ]
                    );
                }
            }}
        />
        <ActionSheet
            ref={o => this.ActionSheet2 = o}
            title={Config.appName}
            options={['Открыть в Яндекс.Картах', 'Открыть в Google Maps', 'Отмена']}
            cancelButtonIndex={2}
            onPress={index => {
                if(index === 0) {
                    let url = 'https://yandex.ru/maps/?text=' + this.state.address;
                    Linking.canOpenURL(url).then(supported => {
                        if (supported) {
                            Linking.openURL(url);
                        } else {
                            console.log("Don't know how to open URI: " + this.props.url);
                        }
                    });
                } else if (index === 1) {
                    let url = 'https://maps.google.com/?q=' + this.state.address;
                    Linking.canOpenURL(url).then(supported => {
                        if (supported) {
                            Linking.openURL(url);
                        } else {
                            console.log("Don't know how to open URI: " + this.props.url);
                        }
                    });
                }
            }}
        />
    </Modal>);
  }
}
