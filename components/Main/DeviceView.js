import React from 'react';
import {
  Platform,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Devices from './../../constants/Devices';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';

export default class DeviceView extends React.Component {

  state = {
    data: null,
	device: null,
	serial: 0,
  };

  UNSAFE_componentWillMount() {
	  
	let name = this.props.data.name;
	let device = null;
	let serial = 'б/н';//this.props.data.id;
	if (name.indexOf('Flex') === 0) {
		console.warn(this.props.data);
		let arr = name.split('-');
		device = Devices[arr[1].toLowerCase()];
		serial = arr[2].split('Sn');
		device.serial = serial;
	}
	
	this.setState({
		data: this.props.data,
		device,
		serial,
	});
  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      data: props.data,
    });
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);
  }

  render() {
	if (this.state.data === null) {
		return null;
	}
    return (<TouchableOpacity style={[{
        marginBottom: Common.getLengthByIPhone7(30),
        padding: Common.getLengthByIPhone7(14),
        paddingBottom: Common.getLengthByIPhone7(11),
        width: Common.getLengthByIPhone7(343),
        minHeight: Common.getLengthByIPhone7(92),
        borderRadius: Common.getLengthByIPhone7(10),
        backgroundColor: 'white',
        shadowColor: '#34374A',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: Common.getLengthByIPhone7(10),
        elevation: 5,
		flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
      }, this.props.style]}
      key={this.props.index}
      onPress={() => {
		Network.queue = [];
		if (this.state.device) {
			Network.currentDevice = this.state.device;
		} else {
			Network.currentDevice = this.props.data;
		}
		
        this.props.navigation.navigate('Device', {data: this.state.data, device: this.state.device});
      }}>
        <View style={{
			alignItems: 'flex-start',
        }}>
			<Text style={{
				width: Common.getLengthByIPhone7(200),
				color: Colors.mainColor,
				fontFamily: 'OpenSans-Bold',
				fontWeight: 'bold',
				textAlign: 'left',
				fontSize: Common.getLengthByIPhone7(14),
				lineHeight: Common.getLengthByIPhone7(19),
				marginBottom: Common.getLengthByIPhone7(5),
				// backgroundColor: 'red',
			}}
			allowFontScaling={false}>
				{this.state.device !== null ? this.state.device.description : 'Назначение неизвестно'}
			</Text>
			<Text style={{
				color: Colors.mainColor,
				fontFamily: 'OpenSans-Bold',
				fontWeight: 'bold',
				textAlign: 'left',
				fontSize: Common.getLengthByIPhone7(18),
				lineHeight: Common.getLengthByIPhone7(24),
				marginBottom: Common.getLengthByIPhone7(5),
			}}
			allowFontScaling={false}>
				{this.state.device !== null ? this.state.device.name : this.state.data.name}
			</Text>
			<Text style={{
				color: '#797979',
				fontFamily: 'OpenSans-Regular',
				fontWeight: '400',
				textAlign: 'right',
				fontSize: Common.getLengthByIPhone7(10),
				lineHeight: Common.getLengthByIPhone7(13),
			}}
			allowFontScaling={false}>
				Синхронизирован: 10.10.2021
			</Text>
        </View>
		<View style={{
			alignItems: 'center',
		}}>
			<Image
				source={this.state.device !== null ? this.state.device.icon : require('./../assets/ic-close2.png')}
				style={{
					resizeMode: 'contain',
					width: Common.getLengthByIPhone7(80),
					height: Common.getLengthByIPhone7(50),
				}}
			/>
			<Text style={{
				color: Colors.mainColor,
				fontFamily: 'OpenSans-Regular',
				fontWeight: 'normal',
				textAlign: 'left',
				fontSize: Common.getLengthByIPhone7(18),
				lineHeight: Common.getLengthByIPhone7(24),
				// marginBottom: Common.getLengthByIPhone7(5),
			}}
			allowFontScaling={false}>
				№{this.state.serial}
			</Text>
		</View>
      </TouchableOpacity>);
    }
}
