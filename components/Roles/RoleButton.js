import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';
import Colors from './../../constants/Colors';
import Common from './../../Utilites/Common';

export default class RoleButton extends React.Component {

    state = {
		active: false,
    };

  UNSAFE_componentWillMount() {
    
  }

  UNSAFE_componentWillUnmount() {}

  constructor(props) {
    super(props);
  }

  render() {

    return (<View style={{

    }}>
		<TouchableOpacity style={{
			width: Common.getLengthByIPhone7(84),
			height: Common.getLengthByIPhone7(78),
			borderRadius: Common.getLengthByIPhone7(10),
			backgroundColor: (this.state.active ? Colors.mainColor : 'white'),
			elevation: 4,
			alignItems: 'center',
			justifyContent: 'center'
		}}
		activeOpacity={1}
		onPressIn={() => {
			this.setState({
				active: true,
			});
		}}
		onPressOut={() => {
			this.setState({
				active: false,
			}, () => {
				setTimeout(() => {
					if (this.props.action) {
						this.props.action();
					}
				}, 100);
			});
		}}
		onPress={() => {
		}}>
			<Image
			source={(this.state.active ? this.props.activeIcon : this.props.inactiveIcon)}
			style={{
				resizeMode: 'contain',
				width: Common.getLengthByIPhone7(46),
				height: Common.getLengthByIPhone7(46),
			}}
			/>
		</TouchableOpacity>
		<Text style={{
			color: Colors.textColor,
			fontFamily: 'OpenSans-SemiBold',
			fontWeight: '600',
			textAlign: 'center',
			fontSize: Common.getLengthByIPhone7(18),
			lineHeight: Common.getLengthByIPhone7(25),
			marginTop: Common.getLengthByIPhone7(17),
		}}
		allowFontScaling={false}>
			{this.props.title}
		</Text>
    </View>);
  }
}
