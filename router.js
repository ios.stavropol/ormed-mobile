import 'react-native-gesture-handler';
import React from "react";
import {isIphoneX} from 'react-native-iphone-x-helper';
import { Platform, Alert, StatusBar, Image, Dimensions, View } from "react-native";
import { createAppContainer, NavigationActions, TabView, TabBarBottom } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import Colors from './constants/Colors';
import Common from './Utilites/Common';

import SplashScreen2 from './screens/SplashScreen2';
import RolesScreen from './screens/RolesScreen';
import LoginScreen from './screens/LoginScreen';
import MainScreen from './screens/MainScreen';
import DeviceScreen from './screens/DeviceScreen';
import DeviceFastSettingsScreen from './screens/DeviceFastSettingsScreen';
import DeviceSettingsScreen from './screens/DeviceSettingsScreen';
import HelpScreen from './screens/HelpScreen';
import PacientsScreen from './screens/PacientsScreen';
import ProfileScreen from './screens/ProfileScreen';
import InfoScreen from './screens/InfoScreen';

import BackButton from './components/Buttons/BackButton';
import ExitButton from './components/Buttons/ExitButton';
import SettingsButton from './components/Buttons/SettingsButton';

const LoginStack = createStackNavigator({
  Roles: {
    screen: RolesScreen,
    navigationOptions: ({navigation}) => {
      return {
        gesturesEnabled: false,
        header: null,
      }
    }
  },
  Login: {
    screen: LoginScreen,
    navigationOptions: ({navigation}) => {
      return {
        gesturesEnabled: true,
        headerLeft: <BackButton navigation={navigation}/>,
        headerRight: <View />,
        titleStyle: {
          alignSelf: 'center',
        },
        headerForceInset: {
          left: 0,
          right: 0,
          top: Platform.OS === 'ios' ? (isIphoneX() ? 44 : 20) : 0,
          bottom: 0,
        },
        headerTitleStyle: {
            color: 'white',
            textAlign: 'center',
            alignSelf:'center',
            flexGrow: 1,
        },
        headerTransparent: false,
        headerStyle: {
            backgroundColor: Colors.mainColor,
        },
        headerTintColor: {
            color: 'red',
        },
      }
    }
  },
}, {
  headerLayoutPreset: 'left',
});

export const LoginTab = createStackNavigator(
  {
    Roles: {
      screen: LoginStack,
      headerMode: 'none',
      header: null,
      navigationOptions: {
        header: null,
        gesturesEnabled: false,
        animationEnabled: false,
      }
    },
  },
  {
    headerMode: "none",
    // mode: "card",
    initialRouteName: 'Roles',
  }
);

// export const SignedIn = createBottomTabNavigator(
//   {
//     MainTab: {
//       screen: createStackNavigator({
//         Main: {
//           screen: MainScreen,
//           navigationOptions: ({navigation}) => {
//             return {
//               gesturesEnabled: false,
//               headerLeft: <View />,
//               headerRight: <View />,
//               headerTitleStyle: {
//                 alignSelf: 'center',
//                 flexGrow: 1,
//               },
//               headerTransparent: false,
//               headerStyle: {
//                 backgroundColor: Colors.mainColor,
//               },
//               headerTintColor: {
//                 color: Colors.textColor,
//               },
//             }
//           }
//         },
//         Device: {
//           screen: DeviceScreen,
//           navigationOptions: ({navigation}) => {
//             return {
//               gesturesEnabled: true,
//               headerLeft: <BackButton navigation={navigation} />,
//               headerRight: <SettingsButton navigation={navigation} />,
//               titleStyle: {
//                 alignSelf: 'center',
//               },
//               headerForceInset: {
//                 left: 0,
//                 right: 0,
//                 top: Platform.OS === 'ios' ? (isIphoneX() ? 44 : 20) : 0,
//                 bottom: 0,
//               },
//               headerTitleStyle: {
//                   color: 'white',
//                   textAlign: 'center',
//                   alignSelf:'center',
//                   flexGrow: 1,
//               },
//               headerTransparent: false,
//               headerStyle: {
//                   backgroundColor: Colors.mainColor,
//               },
//               headerTintColor: {
//                   color: 'red',
//               },
//             }
//           }
//         },
//         DeviceFastSettings: {
//           screen: DeviceFastSettingsScreen,
//           navigationOptions: ({navigation}) => {
//             return {
//               gesturesEnabled: true,
//               headerLeft: <BackButton navigation={navigation} />,
//               headerRight: <View />,
//               titleStyle: {
//                 alignSelf: 'center',
//               },
//               headerForceInset: {
//                 left: 0,
//                 right: 0,
//                 top: Platform.OS === 'ios' ? (isIphoneX() ? 44 : 20) : 0,
//                 bottom: 0,
//               },
//               headerTitleStyle: {
//                   color: 'white',
//                   textAlign: 'center',
//                   alignSelf:'center',
//                   flexGrow: 1,
//               },
//               headerTransparent: false,
//               headerStyle: {
//                 backgroundColor: Colors.mainColor,
//               },
//               headerTintColor: {
//                   color: 'red',
//               },
//             }
//           }
//         },
//         DeviceSettings: {
//           screen: DeviceSettingsScreen,
//           navigationOptions: ({navigation}) => {
//             return {
//               gesturesEnabled: true,
//               headerLeft: <BackButton navigation={navigation} />,
//               headerRight: <View />,
//               titleStyle: {
//                 alignSelf: 'center',
//               },
//               headerForceInset: {
//                 left: 0,
//                 right: 0,
//                 top: Platform.OS === 'ios' ? (isIphoneX() ? 44 : 20) : 0,
//                 bottom: 0,
//               },
//               headerTitleStyle: {
//                   color: 'white',
//                   textAlign: 'center',
//                   alignSelf:'center',
//                   flexGrow: 1,
//               },
//               headerTransparent: false,
//               headerStyle: {
//                 backgroundColor: Colors.mainColor,
//               },
//               headerTintColor: {
//                   color: 'red',
//               },
//             }
//           }
//         },
//         Help: {
//           screen: HelpScreen,
//           navigationOptions: ({navigation}) => {
//             return {
//               gesturesEnabled: true,
//               headerLeft: <BackButton navigation={navigation} />,
//               headerRight: <View />,
//               titleStyle: {
//                 alignSelf: 'center',
//               },
//               headerForceInset: {
//                 left: 0,
//                 right: 0,
//                 top: Platform.OS === 'ios' ? (isIphoneX() ? 44 : 20) : 0,
//                 bottom: 0,
//               },
//               headerTitleStyle: {
//                   color: 'white',
//                   textAlign: 'center',
//                   alignSelf:'center',
//                   flexGrow: 1,
//               },
//               headerTransparent: false,
//               headerStyle: {
//                 backgroundColor: Colors.mainColor,
//               },
//               headerTintColor: {
//                   color: 'red',
//               },
//             }
//           }
//         },
//       }),
//       navigationOptions: ({navigation}) => {
//         return {
//           tabBarIcon: ({ tintColor }) => {
//             return (<Image
//               source={require('./assets/ic-tab1.png')}
//               style={{
//                 resizeMode: 'contain',
//                 width: 24,
//                 height: 24,
//                 tintColor: tintColor,
//               }}
//             />)
//           },
//           gesturesEnabled: false,
//           title: 'Главная',
//           tabBarOnPress: ({ navigation, defaultHandler }) => {
//             let parentNavigation = navigation.dangerouslyGetParent();
//             let prevRoute = parentNavigation.state.routes[parentNavigation.state.index];
//             let nextRoute = navigation.state;
//             // console.log({ prevRoute, nextRoute });
//             defaultHandler();
//           }
//         }
//       }
//     },
//     PacientTab: {
//       screen: createStackNavigator({
//         Main: {
//           screen: PacientsScreen,
//           navigationOptions: ({navigation}) => {
//             return {
//               gesturesEnabled: false,
//               headerLeft: <View />,
//               headerRight: <View />,
//               headerTitleStyle: {
//                 alignSelf: 'center',
//                 flexGrow: 1,
//               },
//               headerTransparent: false,
//               headerStyle: {
//                 backgroundColor: Colors.mainColor,
//               },
//               headerTintColor: {
//                 color: Colors.textColor,
//               },
//             }
//           }
//         },
//       }),
//       navigationOptions: ({navigation}) => {
//         return {
//           tabBarIcon: ({ tintColor }) => {
//             return (<Image
//               source={require('./assets/ic-tab2.png')}
//               style={{
//                 resizeMode: 'contain',
//                 width: 24,
//                 height: 24,
//                 tintColor: tintColor,
//               }}
//             />)
//           },
//           gesturesEnabled: false,
//           title: 'Пациенты',
//           tabBarOnPress: ({ navigation, defaultHandler }) => {
//             let parentNavigation = navigation.dangerouslyGetParent();
//             let prevRoute = parentNavigation.state.routes[parentNavigation.state.index];
//             let nextRoute = navigation.state;
//             // console.log({ prevRoute, nextRoute });
//             defaultHandler();
//           }
//         }
//       }
//     },
//     ProfileTab: {
//       screen: createStackNavigator({
//         Profile: {
//           screen: ProfileScreen,
//           navigationOptions: ({navigation}) => {
//             return {
//               gesturesEnabled: false,
//               headerLeft: <View />,
//               headerRight: <ExitButton navigation={navigation} />,
//               headerTitleStyle: {
//                 alignSelf: 'center',
//                 flexGrow: 1,
//               },
//               headerTransparent: false,
//               headerStyle: {
//                 backgroundColor: Colors.mainColor,
//               },
//               headerTintColor: {
//                 color: Colors.textColor,
//               },
//             }
//           }
//         },
//       }),
//       navigationOptions: ({navigation}) => {
//         return {
//           tabBarIcon: ({ tintColor }) => {
//             return (<Image
//               source={require('./assets/ic-tab3.png')}
//               style={{
//                 resizeMode: 'contain',
//                 width: 24,
//                 height: 24,
//                 // marginTop: 8,
//                 tintColor: tintColor,
//               }}
//             />)
//           },
//           gesturesEnabled: false,
//           title: 'Профиль',
//           tabBarOnPress: ({ navigation, defaultHandler }) => {
//             let parentNavigation = navigation.dangerouslyGetParent();
//             let prevRoute = parentNavigation.state.routes[parentNavigation.state.index];
//             let nextRoute = navigation.state;
//             // console.log({ prevRoute, nextRoute });
//             defaultHandler();
//           }
//         }
//       }
//     },
//   },
//   {
//     // tabBarComponent: (props) => <CustomTabbar navigation={props.navigation} />,
//     lazy: false,
//     initialRouteName: 'MainTab',
//     tabBarOptions: {
//       // activeBackgroundColor: 'white',
//       // inactiveBackgroundColor: 'white',
//       activeTintColor: Colors.mainColor,
//       // inactiveTintColor: Colors.textGrayColor,
//       showLabel: true,
//       showIcon: true,
//       indicatorStyle: {
//         opacity: 0
//       },
//       style: {
//         height: 52,
//       },
//       tabStyle: {
//         flexDirection: 'column',
//         alignItems: 'center',
//         justifyContent: 'center',
//       },
//       labelStyle: {
//         marginBottom: Platform.OS === "android" ? 5 : 5,
//         fontSize: Common.getLengthByIPhone7(10),
//         lineHeight: Common.getLengthByIPhone7(12),
//         fontWeight: 'normal',
//         fontFamily: 'Rubik-Regular',
//       },
//     },
//     navigationOptions: {
//       scrollEnabled: false,
//       headerMode: "none",
//       mode: 'card',
//       configureTransition: (currentTransitionProps,nextTransitionProps) => ({
//         duration: 0,
//         timing: Animated.timing,
//         easing: Easing.step0,
//      })
//     },
//   }
// );

export const SignedIn = createStackNavigator({
  Main: {
    screen: MainScreen,
    navigationOptions: ({navigation}) => {
      return {
        gesturesEnabled: false,
        headerLeft: <View />,
        headerRight: <View />,
        headerTitleStyle: {
          alignSelf: 'center',
          flexGrow: 1,
        },
        headerTransparent: false,
        headerStyle: {
          backgroundColor: Colors.mainColor,
        },
        headerTintColor: {
          color: Colors.textColor,
        },
      }
    }
  },
  Info: {
    screen: InfoScreen,
    navigationOptions: ({navigation}) => {
      return {
        gesturesEnabled: false,
        header: null,
      }
    }
  },
  Device: {
    screen: DeviceScreen,
    navigationOptions: ({navigation}) => {
      return {
        gesturesEnabled: true,
        headerLeft: <BackButton navigation={navigation} />,
        headerRight: <SettingsButton navigation={navigation} />,
        titleStyle: {
          alignSelf: 'center',
        },
        headerForceInset: {
          left: 0,
          right: 0,
          top: Platform.OS === 'ios' ? (isIphoneX() ? 44 : 20) : 0,
          bottom: 0,
        },
        headerTitleStyle: {
            color: 'white',
            textAlign: 'center',
            alignSelf:'center',
            flexGrow: 1,
        },
        headerTransparent: false,
        headerStyle: {
            backgroundColor: Colors.mainColor,
        },
        headerTintColor: {
            color: 'red',
        },
      }
    }
  },
  DeviceFastSettings: {
    screen: DeviceFastSettingsScreen,
    navigationOptions: ({navigation}) => {
      return {
        gesturesEnabled: true,
        headerLeft: <BackButton navigation={navigation} />,
        headerRight: <View />,
        titleStyle: {
          alignSelf: 'center',
        },
        headerForceInset: {
          left: 0,
          right: 0,
          top: Platform.OS === 'ios' ? (isIphoneX() ? 44 : 20) : 0,
          bottom: 0,
        },
        headerTitleStyle: {
            color: 'white',
            textAlign: 'center',
            alignSelf:'center',
            flexGrow: 1,
        },
        headerTransparent: false,
        headerStyle: {
          backgroundColor: Colors.mainColor,
        },
        headerTintColor: {
            color: 'red',
        },
      }
    }
  },
  DeviceSettings: {
    screen: DeviceSettingsScreen,
    navigationOptions: ({navigation}) => {
      return {
        gesturesEnabled: true,
        headerLeft: <BackButton navigation={navigation} />,
        headerRight: <View />,
        titleStyle: {
          alignSelf: 'center',
        },
        headerForceInset: {
          left: 0,
          right: 0,
          top: Platform.OS === 'ios' ? (isIphoneX() ? 44 : 20) : 0,
          bottom: 0,
        },
        headerTitleStyle: {
            color: 'white',
            textAlign: 'center',
            alignSelf:'center',
            flexGrow: 1,
        },
        headerTransparent: false,
        headerStyle: {
          backgroundColor: Colors.mainColor,
        },
        headerTintColor: {
            color: 'red',
        },
      }
    }
  },
  Help: {
    screen: HelpScreen,
    navigationOptions: ({navigation}) => {
      return {
        gesturesEnabled: true,
        headerLeft: <BackButton navigation={navigation} />,
        headerRight: <View />,
        titleStyle: {
          alignSelf: 'center',
        },
        headerForceInset: {
          left: 0,
          right: 0,
          top: Platform.OS === 'ios' ? (isIphoneX() ? 44 : 20) : 0,
          bottom: 0,
        },
        headerTitleStyle: {
            color: 'white',
            textAlign: 'center',
            alignSelf:'center',
            flexGrow: 1,
        },
        headerTransparent: false,
        headerStyle: {
          backgroundColor: Colors.mainColor,
        },
        headerTintColor: {
            color: 'red',
        },
      }
    }
  },
});

export const AllScreens = createStackNavigator(
  {
    Splash: {
      screen: SplashScreen2,
      headerMode: 'none',
      header: null,
      navigationOptions: ({navigation}) => {
        return {
          gesturesEnabled: false,
        }
      }
    },
    LoginTab: {
      screen: LoginTab,
      navigationOptions: {
        gesturesEnabled: false,
      }
    },
    Tabs: {
      screen: SignedIn,
      navigationOptions: {
        gesturesEnabled: false,
      }
    },
  },
  {
    headerMode: "none",
    mode: "modal",
    initialRouteName: 'Splash',
  }
);

export const createRootNavigator = () => {

  return createAppContainer(AllScreens);
};
